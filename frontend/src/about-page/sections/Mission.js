import banner from "../../images/about-banner.png";
import { Button } from "../../components/Buttons";

var stringSubText = `We've collected investment information from politicans to add transparency and accountability in the United States Congress.`;

export default function Mission() {
  return (
    <div style={{ display: "flex", flexFlow: "column", height: "100vh" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          paddingLeft: 50,
          paddingRight: 50,
          flexGrow: 1,
          flexShrink: 1,
          flexBasis: "auto",
        }}
      >
        <div style={{ fontSize: 60, fontWeight: 500, marginBottom: -24 }}>
          {"Introducing"}
        </div>
        <div style={{ fontSize: 60, fontWeight: 500 }}>{"Capitol Capital"}</div>
        <div class="h4" style={{ width: "50%" }}>
          {stringSubText}
        </div>
        <a href="https://documenter.getpostman.com/view/25801509/2s935uH1oE">
          <Button label={"View API"} />
        </a>
      </div>
      <img
        style={{
          flexGrow: 0,
          flexShrink: 1,
          flexBasis: 100,
          height: 300,
          objectFit: "cover",
          border: "2px #262626 solid",
        }}
        src={
          //"https://res.cloudinary.com/dk/image/upload/q_80,c_fill,w_892,h_380,f_auto/washington-dc/united-states-capitol-aw-crop.jpg"
          // "https://res.cloudinary.com/dk/image/upload/q_80,c_fill,w_892,h_380,f_auto/vienna/burgtheater-aw-crop.jpg"
          "https://datavizcatalogue.com/methods/images/top_images/SVG/candlestick_chart.svg"
        }
        alt="IMG"
      />
    </div>
  );
}
