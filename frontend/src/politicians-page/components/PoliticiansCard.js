import { Card, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { getRandomMoney } from "../../components/Money";

export default function PoliticiansCard(props) {
  const {
    name,
    party,
    holdings,
    id,
    profileUrl,
    birthDate,
    firstElected,
  } = props;
  const createMarkup = (html) => {
    return { __html: html };
  };
  return (
    <Card>
      <Card.Img
        variant="top"
        style={{ height: 200, objectFit: "cover" }}
        src={profileUrl}
      />
      <Card.Body>
        <Card.Title>
          <span dangerouslySetInnerHTML={createMarkup(name)}></span>
        </Card.Title>
        <Card.Subtitle
          style={{
            fontWeight: 700,
            color: party === "D" ? "#0015BC" : "#DE0100",
          }}
        >
          <span dangerouslySetInnerHTML={createMarkup(party)}></span>
        </Card.Subtitle>
        <Card.Text>
          {`Total holdings:`}{" "}
          <span style={{ color: "#278225" }}>{getRandomMoney(200000)}</span>
        </Card.Text>
        <Card.Text>
          {`Debt:`}{" "}
          <span style={{ color: "#EA5D5D" }}>{getRandomMoney(10000)}</span>
        </Card.Text>
        <Card.Text>
          <span
            dangerouslySetInnerHTML={createMarkup(
              `Date of Birth: ${birthDate}`
            )}
          ></span>
        </Card.Text>
        <Card.Text>{`First Elected: ${firstElected}`}</Card.Text>
        <LinkContainer to={`/politicians/${id}`}>
          <Button variant="primary">View</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}
