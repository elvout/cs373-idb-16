import { useState, useEffect } from "react";
import Spinner from "react-bootstrap/Spinner";
import { Button } from "react-bootstrap";
import SearchBar from "./Search";
import { useNavigate, useSearchParams, useLocation } from "react-router-dom";
import { PaginationControl } from "react-bootstrap-pagination-control";

function ServerError(props) {
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        fontSize: 20,
        marginTop: "30vh",
      }}
    >
      {"Could not load resources! (Our backend is down)  :'("}
    </div>
  );
}

function PageNotFound(props) {
  const { url, label } = props;
  return (
    <div
      style={{
        flex: 1,
        height: "50vh",
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <img style={{ height: 200, width: 300, objectFit: "cover" }} src={url} />
      <div>{label}</div>
    </div>
  );
}
function Loading(props) {
  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        display: "flex",
        flexDirection: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Spinner animation="border" role="status">
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    </div>
  );
}

export default function CardPage(props) {
  const {
    cards,
    search,
    setSearch,
    filterComponents,
    reset,
    page,
    numPerPage,
    changePage,
  } = props;
  const [timedOut, setTimedOut] = useState(false);

  useEffect(() => {
    if (!cards) {
      setTimeout(() => {
        if (!cards) {
          setTimedOut(true);
        }
      }, 5000);
    }
  }, []);

  if (!cards) {
    return timedOut ? <ServerError /> : <Loading />;
  }
  return (
    <div style={{ marginTop: 10 }}>
      <SearchBar search={search} setSearch={setSearch} />
      <div
        style={{ display: "flex", justifyContent: "center", marginBottom: 10 }}
      >
        {filterComponents.map((obj) => {
          return <div style={{ margin: 5 }}>{obj}</div>;
        })}
        <div style={{ margin: 5 }}>
          <Button onClick={reset}>{"Reset"}</Button>
        </div>
      </div>
      <PaginationControl
        page={page}
        between={3}
        total={100}
        limit={numPerPage}
        changePage={changePage}
        ellipsis={1}
      />
      {cards.length == 0 ? (
        <PageNotFound />
      ) : (
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr 1fr",
            gridGap: "20px",
            padding: 20,
          }}
        >
          {cards}
        </div>
      )}
    </div>
  );
}
