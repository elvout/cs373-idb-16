import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

export default function VBarChart(props) {
  const { data } = props;
  return (
    <ResponsiveContainer width={"90%"} height={400}>
      <BarChart width={1000} height={500} data={data} barGap={10}>
        <XAxis
          dataKey={"stock"}
          angle={45}
          interval={0}
          textAnchor={"start"}
          height={120}
        />
        <YAxis />
        <Tooltip labelStyle={{ color: "black" }} />
        <Bar dataKey={"amount"} fill={"#4b8cc4"} bar />
      </BarChart>
    </ResponsiveContainer>
  );
}
