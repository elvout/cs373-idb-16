import gitlab from "../../images/gitlab.png";
import postman from "../../images/postman.png";

function DocumentationComponent(props) {
  const { name, link, logo } = props;
  return (
    <a href={link} style={{ marginLeft: 30, textDecoration: "none" }}>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <img style={{ width: 40, height: 40 }} src={logo} alt="IMG" />
        <div
          style={{
            marginLeft: 10,
            fontWeight: 400,
            fontSize: 24,
            color: "#FFFFFF",
            marginRight: 60,
          }}
        >
          {name}
        </div>
      </div>
    </a>
  );
}

export default function Documentation() {
  return (
    <div style={{ marginTop: 100, backgroundColor: "#262626" }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          paddingTop: 30,
          paddingBottom: 30,
          paddingLeft: 20,
        }}
      >
        <div
          style={{
            fontWeight: 400,
            fontSize: 30,
            color: "#FFFFFF",
            marginRight: 60,
          }}
        >
          Documentation:
        </div>
        <DocumentationComponent
          name={"Gitlab"}
          logo={gitlab}
          link={"https://gitlab.com/elvout/cs373-idb-16"}
        />
        <DocumentationComponent
          name={"Postman"}
          logo={postman}
          link={"https://documenter.getpostman.com/view/25801509/2s935uH1oE"}
        />
      </div>
    </div>
  );
}
