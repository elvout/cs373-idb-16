from sqlalchemy import (
    ARRAY,
    Column,
    Float,
    Integer,
    String,
    create_engine,
    delete,
    exc,
    select,
    func,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

Base = declarative_base()
engine = create_engine(
    "postgresql://capitolcapital:capitolcapital@capitolcapital.cdjnxytegtb2.us-east-1.rds.amazonaws.com:5432/capitolcapital"
)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
mysession = scoped_session(Session)

# engine = create_engine('postgresql://capitolcapital:capitolcapital@capitolcapital1.cn948zvhvbtw.us-east-1.rds.amazonaws.com:5432/capitolcapital')

"""
declare classes for database
"""


class politicians(Base):  # type: ignore
    __tablename__ = "politicians"
    PoliticianID = Column("PoliticianID", String, primary_key=True)
    Name = Column("Name", String)
    Gender = Column("Gender", String)
    Party = Column("Party", String)
    DateofBirth = Column("DateofBirth", String)
    FirstElected = Column("FirstElected", Integer)
    CashonHand = Column("CashonHand", Float)
    Debt = Column("Debt", Float)
    ImgURL = Column("ImgURL", String)
    BannerURL = Column("BannerURL", String)
    ProfileURL = Column("ProfileURL", String)
    TwitterAccount = Column("TwitterAccount", String)
    StocksTraded = Column("StocksTraded", ARRAY(String))
    CommitteesMembership = Column("CommitteesMembership", ARRAY(String))

    def __init__(
        self,
        PoliticianID,
        Name,
        Gender,
        Party,
        DateofBirth,
        FirstElected,
        CashonHand,
        Debt,
        ImgURL,
        BannerURL,
        ProfileURL,
        TwitterAccount,
        StocksTraded,
        CommitteesMembership,
    ):
        self.PoliticianID = PoliticianID
        self.Name = Name
        self.Gender = Gender
        self.Party = Party
        self.DateofBirth = DateofBirth
        self.FirstElected = FirstElected
        self.CashonHand = CashonHand
        self.Debt = Debt
        self.ImgURL = ImgURL
        self.BannerURL = BannerURL
        self.ProfileURL = ProfileURL
        self.TwitterAccount = TwitterAccount
        self.StocksTraded = StocksTraded
        self.CommitteesMembership = CommitteesMembership


class stocks(Base):  # type: ignore
    __tablename__ = "stocks"
    StockID = Column("StockID", String, primary_key=True)
    Name = Column("Name", String)
    StockPrice = Column("StockPrice", Float)
    MarketCap = Column("MarketCap", Float)
    Sector = Column("Sector", String)
    Industry = Column("Industry", String)
    ImgURL = Column("ImgURL", String)
    LogoURL = Column("LogoURL", String)
    TwitterAccount = Column("TwitterAccount", String)
    Address = Column("Address", String)
    Description = Column("Description", String)
    TradedPoliticians = Column("TradedPoliticians", ARRAY(String))
    TradedCommittees = Column("TradedCommittees", ARRAY(String))

    def __init__(
        self,
        StockID,
        Name,
        StockPrice,
        MarketCap,
        Sector,
        Industry,
        ImgURL,
        LogoURL,
        TwitterAccount,
        Address,
        Description,
        TradedPoliticians,
        TradedCommittees,
    ):
        self.StockID = StockID
        self.Name = Name
        self.StockPrice = StockPrice
        self.MarketCap = MarketCap
        self.Sector = Sector
        self.Industry = Industry
        self.ImgURL = ImgURL
        self.LogoURL = LogoURL
        self.TwitterAccount = TwitterAccount
        self.Address = Address
        self.Description = Description
        self.TradedPoliticians = TradedPoliticians
        self.TradedCommittees = TradedCommittees


class committees(Base):  # type: ignore
    __tablename__ = "committees"
    CommitteesID = Column("CommitteesID", String, primary_key=True)
    Congress = Column("Congress", Integer)
    Name = Column("Name", String)
    Chamber = Column("Chamber", String)
    StartDate = Column("StartDate", Integer)
    URL = Column("URL", String)
    Chair = Column("Chair", String)
    NumberofMembers = Column("NumberofMembers", Integer)
    LogoURL = Column("LogoURL", String)
    ImgURL = Column("ImgURL", String)
    TwitterAccount = Column("TwitterAccount", String)
    Investments = Column("Investments", Float)
    Members = Column("Members", ARRAY(String))
    StocksTraded = Column("StocksTraded", ARRAY(String))

    def __init__(
        self,
        CommitteesID,
        Congress,
        Name,
        Chamber,
        StartDate,
        URL,
        Chair,
        NumberofMembers,
        LogoURL,
        ImgURL,
        TwitterAccount,
        Members,
        StocksTraded,
    ):
        self.CommitteesID = CommitteesID
        self.Congress = Congress
        self.Name = Name
        self.Chamber = Chamber
        self.StartDate = StartDate
        self.URL = URL
        self.Chair = Chair
        self.NumberofMembers = NumberofMembers
        self.LogoURL = LogoURL
        self.ImgURL = ImgURL
        self.TwitterAccount = TwitterAccount
        self.Members = Members
        self.StocksTraded = StocksTraded


"""
Insert
"""


def addPolitician(
    PoliticianID,
    Name,
    Gender,
    Party,
    DateofBirth,
    FirstElected,
    CashonHand,
    Debt,
    ImgURL,
    BannerURL,
    ProfileURL,
    TwitterAccount,
    StocksTraded,
    CommitteesMembership,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(
            politicians(
                PoliticianID,
                Name,
                Gender,
                Party,
                DateofBirth,
                FirstElected,
                CashonHand,
                Debt,
                ImgURL,
                BannerURL,
                ProfileURL,
                TwitterAccount,
                StocksTraded,
                CommitteesMembership,
            )
        )
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


def addStocks(
    StockID,
    Name,
    StockPrice,
    MarketCap,
    Sector,
    Industry,
    ImgURL,
    LogoURL,
    TwitterAccount,
    Address,
    Description,
    TradedPoliticians,
    TradedCommittees,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(
            stocks(
                StockID,
                Name,
                StockPrice,
                MarketCap,
                Sector,
                Industry,
                ImgURL,
                LogoURL,
                TwitterAccount,
                Address,
                Description,
                TradedPoliticians,
                TradedCommittees,
            )
        )
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


def addCommittees(
    CommitteesID,
    Congress,
    Name,
    Chamber,
    StartDate,
    URL,
    Chair,
    NumberofMembers,
    LogoURL,
    ImgURL,
    TwitterAccount,
    Investments,
    Members,
    StocksTraded,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(
            committees(
                CommitteesID,
                Congress,
                Name,
                Chamber,
                StartDate,
                URL,
                Chair,
                NumberofMembers,
                LogoURL,
                ImgURL,
                TwitterAccount,
                Members,
                StocksTraded,
            )
        )
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


"""
Drop Table
"""


# drop everything in table
def dropAllRows():
    try:
        stmt = delete(committees)
        with engine.connect() as conn:
            conn.execute(stmt)
        stmt = delete(stocks)
        with engine.connect() as conn:
            conn.execute(stmt)
        stmt = delete(politicians)
        with engine.connect() as conn:
            conn.execute(stmt)
    except exc.SQLAlchemyError:
        print("Drop Failed")


"""
For API Implementation
"""

"""Paging"""


def getCommittees(start, end, asc, o, s, number_min, number_max, cham):
    search = "%{}%".format(s)
    a = committees.NumberofMembers >= number_min
    b = committees.NumberofMembers <= number_max
    if cham == "senate" or cham == "Senate":
        c = committees.Chamber == "Senate"
    if cham == "house" or cham == "House":
        c = committees.Chamber == "House"
    if cham == "joint" or cham == "Joint":
        c = committees.Chamber == "Joint"
    try:
        t = ()
        if o == "Congress":
            if asc == 1:
                k = committees.Congress
            else:
                k = committees.Congress.desc()
        elif o == "NumberofMembers":
            if asc == 1:
                k = committees.NumberofMembers
            else:
                k = committees.NumberofMembers.desc()
        elif o == "Investments":
            if asc == 1:
                k = committees.Investments
            else:
                k = committees.Investments.desc()
        elif o == "Year":
            if asc == 1:
                k = committees.StartDate
            else:
                k = committees.StartDate.desc()
        else:
            if asc == 1:
                k = committees.CommitteesID
            else:
                k = committees.CommitteesID.desc()
        if cham != "":
            stmt = (
                select(committees)
                .limit(int(end) - int(start))
                .offset(start)
                .order_by(k)
                .filter(committees.Name.ilike(search))
                .where(a & b & c)
            )
        else:
            stmt = (
                select(committees)
                .limit(int(end) - int(start))
                .offset(start)
                .order_by(k)
                .filter(committees.Name.ilike(search))
                .where(a & b)
            )
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError as e:
        print(e)


def getCommitteesCount(ser, minNumber, maxNumber, cham):
    search = "%{}%".format(ser)
    a = committees.NumberofMembers >= minNumber
    b = committees.NumberofMembers <= maxNumber
    c = committees.NumberofMembers <= maxNumber
    if cham == "senate" or cham == "Senate":
        c = committees.Chamber == "Senate"
    if cham == "house" or cham == "House":
        c = committees.Chamber == "House"
    if cham == "joint" or cham == "Joint":
        c = committees.Chamber == "Joint"
    try:
        if cham != "":
            stmt = (
                select([func.count()])
                .filter(committees.Name.ilike(search))
                .where(a & b & c)
            )
        else:
            stmt = (
                select([func.count()])
                .filter(committees.Name.ilike(search))
                .where(a & b)
            )
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t = row[0]
        return t
    except exc.SQLAlchemyError as e:
        print(e)


def getStocks(start, end, asc, o, s, min, max, sector):
    search = "%{}%".format(s)
    a = stocks.StockPrice >= min
    b = stocks.StockPrice <= max
    c = stocks.Sector == sector
    try:
        t = ()
        if o == "StockPrice":
            if asc == 1:
                k = stocks.StockPrice
            else:
                k = stocks.StockPrice.desc()
        elif o == "MarketCap":
            if asc == 1:
                k = stocks.MarketCap
            else:
                k = stocks.MarketCap.desc()
        else:
            if asc == 1:
                k = stocks.StockID
            else:
                k = stocks.StockID.desc()
        if sector != "":
            stmt = (
                select(stocks)
                .limit(int(end) - int(start))
                .offset(start)
                .order_by(k)
                .filter(
                    stocks.Name.ilike(search)
                    | stocks.Sector.ilike(search)
                    | stocks.StockID.ilike(search)
                    | stocks.Industry.ilike(search)
                )
                .where(a & b & c)
            )
        else:
            stmt = (
                select(stocks)
                .limit(int(end) - int(start))
                .offset(start)
                .order_by(k)
                .filter(
                    stocks.Name.ilike(search)
                    | stocks.Sector.ilike(search)
                    | stocks.StockID.ilike(search)
                    | stocks.Industry.ilike(search)
                )
                .where(a & b)
            )
        select
        with engine.connect() as conn:
            print(conn.execute(stmt))
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getStocksCount(ser, minNumber, maxNumber, sector):
    search = "%{}%".format(ser)
    a = stocks.StockPrice >= minNumber
    b = stocks.StockPrice <= maxNumber
    c = stocks.Sector == sector
    try:
        t = ()
        if sector != "":
            stmt = (
                select([func.count()])
                .filter(
                    stocks.Name.ilike(search)
                    | stocks.Sector.ilike(search)
                    | stocks.StockID.ilike(search)
                    | stocks.Industry.ilike(search)
                )
                .where(a & b & c)
            )
        else:
            stmt = (
                select([func.count()])
                .filter(
                    stocks.Name.ilike(search)
                    | stocks.Sector.ilike(search)
                    | stocks.StockID.ilike(search)
                    | stocks.Industry.ilike(search)
                )
                .where(a & b)
            )
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t = row[0]
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getPoliticians(start, end, asc, o, s, number_min, numberd_max, g, p):
    a = politicians.CashonHand >= number_min
    b = politicians.CashonHand <= numberd_max
    if g != "":
        c = politicians.Gender == g
    else:
        c = a
    if p != "":
        d = politicians.Party == p
    else:
        d = a
    search = "%{}%".format(s)
    try:
        t = ()
        if o == "FirstElected":
            if asc == 1:
                k = politicians.FirstElected
            else:
                k = politicians.FirstElected.desc()
        elif o == "CashonHand":
            if asc == 1:
                k = politicians.CashonHand
            else:
                k = politicians.CashonHand.desc()
        elif o == "Debt":
            if asc == 1:
                k = politicians.Debt
            else:
                k = politicians.Debt.desc()
        else:
            if asc == 1:
                k = politicians.PoliticianID
            else:
                k = politicians.PoliticianID.desc()
        stmt = (
            select(politicians)
            .limit(int(end) - int(start))
            .offset(start)
            .order_by(k)
            .filter(politicians.Name.ilike(search))
            .where(a & b & c & d)
        )
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getPoliticiansCount(s, minNumber, maxNumber, g, p):
    a = politicians.CashonHand >= minNumber
    b = politicians.CashonHand <= maxNumber
    if g != "":
        c = politicians.Gender == g
    else:
        c = a
    if p != "":
        d = politicians.Party == p
    else:
        d = a
    search = "%{}%".format(s)
    try:
        t = ()

        stmt = (
            select([func.count()])
            .filter(politicians.Name.ilike(search))
            .where(a & b & c & d)
        )
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t = row[0]
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


"""By ID"""


def getPoliticiansbyID(val):
    try:
        t = ()
        stmt = select(politicians).where(politicians.PoliticianID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getStocksbyID(val):
    try:
        t = ()
        stmt = select(stocks).where(stocks.StockID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getCommitteesbyID(val):
    try:
        t = ()
        stmt = select(committees).where(committees.CommitteesID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


"""Find Connection"""

"""Will implement in the future"""


# Get a connection from database
def getPoliticiansMembership(val):
    try:
        t = ()
        stmt = select(politicians.CommitteesMembership).where(
            politicians.PoliticianID == val
        )
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getPoliticiansStock(val):
    try:
        t = ()
        stmt = select(politicians.StocksTraded).where(politicians.PoliticianID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getstocksToCommittees(val):
    try:
        t = ()
        stmt = select(stocks.TradedCommittees).where(stocks.StockID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getstocksToCPoliticians(val):
    try:
        t = ()
        stmt = select(stocks.TradedPoliticians).where(stocks.StockID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getCommitteesMembership(val):
    try:
        t = ()
        stmt = select(committees.Members).where(committees.CommitteesID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getComToStock(val):
    try:
        t = ()
        stmt = select(ctos.StockID).where(ctos.CommitteesID == val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getComtoStock2(val):
    try:
        for r in getComToStock(val)[0][0]:
            print(getStocksbyID(r))
    except exc.SQLAlchemyError:
        print("Select Failed")


"""Phase 3 Implementations"""


def getStocksPriceSearch(val):
    try:
        t = ()
        stmt = select(stocks).where(stocks.StockPrice > val)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


def getStocksPriceSearchOrder(val):
    try:
        t = ()
        a = stocks.StockPrice >= 100
        b = stocks.StockPrice <= 101
        stmt = select(stocks).where(a & b)
        select
        with engine.connect() as conn:
            for row in conn.execute(stmt):
                t += (row,)
        return t
    except exc.SQLAlchemyError:
        print("Select Failed")


"""
Connection Tables
"""


class ptos(Base):  # type: ignore
    __tablename__ = "ptos"
    pk = Column("pk", String, primary_key=True)
    PoliticianID = Column("PoliticianID", String)
    StockID = Column("StockID", String)

    def __init__(self, a, b, c):
        self.pk = a
        self.PoliticianID = b
        self.StockID = c


def addptos(
    pk,
    PoliticianID,
    StockID,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(ptos(pk, PoliticianID, StockID))
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


class ptoc(Base):  # type: ignore
    __tablename__ = "ptoc"
    pk = Column("pk", String, primary_key=True)
    PoliticianID = Column("PoliticianID", String)
    CommitteesID = Column("CommitteesID", String)

    def __init__(self, a, b, c):
        self.pk = a
        self.PoliticianID = b
        self.CommitteesID = c


def addptoc(
    pk,
    PoliticianID,
    CommitteesID,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(ptoc(pk, PoliticianID, CommitteesID))
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


class stop(Base):  # type: ignore
    __tablename__ = "stop"
    pk = Column("pk", String, primary_key=True)
    StockID = Column("StockID", String)
    PoliticianID = Column("PoliticianID", String)

    def __init__(self, a, b, c):
        self.pk = a
        self.StockID = b
        self.PoliticianID = c


def addstop(
    pk,
    StockID,
    PoliticianID,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(stop(pk, StockID, PoliticianID))
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


class stoc(Base):  # type: ignore
    __tablename__ = "stoc"
    pk = Column("pk", String, primary_key=True)
    StockID = Column("StockID", String)
    CommitteesID = Column("CommitteesID", String)

    def __init__(self, a, b, c):
        self.pk = a
        self.StockID = b
        self.CommitteesID = c


def addstoc(
    pk,
    StockID,
    CommitteesID,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(stoc(pk, StockID, CommitteesID))
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


class ctos(Base):  # type: ignore
    __tablename__ = "ctos"
    pk = Column("pk", String, primary_key=True)
    CommitteesID = Column("CommitteesID", String)
    StockID = Column("StockID", String)

    def __init__(self, a, b, c):
        self.pk = a
        self.CommitteesID = b
        self.StockID = c


def addctos(
    pk,
    CommitteesID,
    StockID,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(ctos(pk, CommitteesID, StockID))
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


class ctop(Base):  # type: ignore
    __tablename__ = "ctop"
    pk = Column("pk", String, primary_key=True)
    CommitteesID = Column("CommitteesID", String)
    PoliticianID = Column("PoliticianID", String)

    def __init__(self, a, b, c):
        self.pk = a
        self.CommitteesID = b
        self.PoliticianID = c


def addctop(
    pk,
    CommitteesID,
    PoliticianID,
):
    try:
        Session = sessionmaker()
        Session.configure(bind=engine)
        session = Session()
        session.add(ctop(pk, CommitteesID, PoliticianID))
        session.commit()
        session.close()
    except exc.SQLAlchemyError:
        print("Insertion Failed")


# for table creation
"""
from sqlalchemy import Table, MetaData
#engine = create_engine('postgresql://capitolcapital:capitolcapital@capitolcapital1.cn948zvhvbtw.us-east-1.rds.amazonaws.com:5432/capitolcapital')
meta = MetaData()
test = Table(
    "politicians", meta,
    Column("PoliticianID", String, primary_key=True),
    Column("Name", String),
    Column("Gender", String),
    Column("Party", String),
    Column("DateofBirth", String),
    Column("FirstElected", Integer),
    Column("CashonHand", Float),
    Column("Debt", Float),
    Column("ImgURL", String),
    Column("BannerURL", String),
    Column("ProfileURL", String),
    Column("TwitterAccount", String),
    Column("StocksTraded", ARRAY(String)),
    Column("CommitteesMembership", ARRAY(String))
)
test1 = Table(
    "stocks", meta,
    Column("StockID", String, primary_key=True),
    Column("Name", String),
    Column("StockPrice", Float),
    Column("MarketCap", Float),
    Column("Sector", String),
    Column("Industry", String),
    Column("ImgURL", String),
    Column("LogoURL", String),
    Column("TwitterAccount", String),
    Column("Address", String),
    Column("Description", String),
    Column("TradedPoliticians", ARRAY(String)),
    Column("TradedCommittees", ARRAY(String))
)
test2 = Table(
    "committees", meta,
    Column("CommitteesID", String, primary_key=True),
    Column("Congress", Integer),
    Column("Name", String),
    Column("Chamber", String),
    Column("StartDate", Integer),
    Column("URL", String),
    Column("Chair", String),
    Column("NumberofMembers", Integer),
    Column("LogoURL", String),
    Column("ImgURL", String),
    Column("TwitterAccount", String),
    Column("Investments", Float),
    Column("Members", ARRAY(String)),
    Column("StocksTraded", ARRAY(String))
)
meta.create_all(engine)
"""

# Insert Values
"""
addPolitician(engine,"A000370","Alma Adams","F","D","1946-05-27","1987",3593728.76,0,"https://adams.house.gov/sites/evo-subsites/adams.house.gov/files/styles/evo_image_large_960/public/evo-media-image/vi-hakeem-asa-smol.jpg?h=23fe37d3&itok=4527KvyK","https://adams.house.gov/sites/evo-subsites/adams-evo.house.gov/files/evo-media-image/Alma_Adams_official_portrait.jpg",["IBM", "TSLA"],["HSHA"])
addPolitician(engine,"B001230","Tammy Baldwin","F","D","1962-02-11","1999",3038018.37,0,"https://www.baldwin.senate.gov/assets/images/bgs/home-background-1.jpg","https://cdn.britannica.com/03/181903-050-6AF2FAD7/Tammy-Baldwin.jpg?w=400&h=300&c=crop",["IBM"],["HSHA", "HSAG", "HSSO"])
addPolitician(engine,"B001261","John Barrasso","M","R","1952-07-21","1995",5847520.42,0,"https://www.baldwin.senate.gov/assets/images/bgs/home-background-1.jpg","https://www.gannett-cdn.com/presto/2019/03/04/USAT/f2d892c7-857c-40d2-aed5-bae69688be75-green.oppose.JPG?width=660&height=440&fit=crop&format=pjpg&auto=webp",["IBM", "TSLA","NKE"],["HSHA"])

addStocks(engine,"IBM","International Business Machines",24.23,121541001000,"TECHNOLOGY","COMPUTER & OFFICE EQUIPMENT","https://www.techrepublic.com/wp-content/uploads/2022/05/ibm-announces-4000-qubits.jpeg","https://logo.clearbit.com/ibm.com","1 NEW ORCHARD ROAD, ARMONK, NY, US","International Business Machines Corporation (IBM) is an American multinational technology company headquartered in Armonk, New York, with operations in over 170 countries. The company began in 1911, founded in Endicott, New York, as the Computing-Tabulating-Recording Company (CTR) and was renamed International Business Machines in 1924. IBM is incorporated in New York. IBM produces and sells computer hardware, middleware and software, and provides hosting and consulting services in areas ranging from mainframe computers to nanotechnology. IBM is also a major research organization, holding the record for most annual U.S. patents generated by a business (as of 2020) for 28 consecutive years. Inventions by IBM include the automated teller machine (ATM), the floppy disk, the hard disk drive, the magnetic stripe card, the relational database, the SQL programming language, the UPC barcode, and dynamic random-access memory (DRAM). The IBM mainframe, exemplified by the System/360, was the dominant computing platform during the 1960s and 1970s.",["A000370","B001230", "B001261"], ["HSHA", "HSAG", "HSSO"] )
addStocks(engine,"TSLA","Tesla Inc",20.45,58778585000,"MANUFACTURING","MOTOR VEHICLES & PASSENGER CAR BODIES","https://tesla-cdn.thron.com/delivery/public/image/tesla/458cfaaf-de1e-47e0-867e-cb78c1993db3/bvlatuR/std/1200x628/Model-X-Social?quality=auto-medium&format=autoo","https://logo.clearbit.com/tesla.com","3500 DEER CREEK RD, PALO ALTO, CA, US","Tesla, Inc. is an American electric vehicle and clean energy company based in Palo Alto, California. Tesla's current products include electric cars, battery energy storage from home to grid-scale, solar panels and solar roof tiles, as well as other related products and services. In 2020, Tesla had the highest sales in the plug-in and battery electric passenger car segments, capturing 16% of the plug-in market (which includes plug-in hybrids) and 23% of the battery-electric (purely electric) market. Through its subsidiary Tesla Energy, the company develops and is a major installer of solar photovoltaic energy generation systems in the United States. Tesla Energy is also one of the largest global suppliers of battery energy storage systems, with 3 GWh of battery storage supplied in 2020.",["A000370","B001261"],["HSHA"] )
addStocks(engine,"NKE","Nike Inc",9.85,189524951000,"MANUFACTURING","RUBBER & PLASTICS FOOTWEAR","https://images.complex.com/complex/images/c_crop,h_507,w_901,x_50,y_40/c_fill,dpr_auto,f_auto,q_auto,w_1400/fl_lossy,pg_1/mqomurtg7w9gg818ofof/nike-snkrs-box?fimg-ssr-default","https://logo.clearbit.com/nike.com","ONE BOWERMAN DR, BEAVERTON, OR, US","Nike, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services. The company is headquartered near Beaverton, Oregon, in the Portland metropolitan area. It is the world's largest supplier of athletic shoes and apparel and a major manufacturer of sports equipment.",["B001261"],["HSHA"] )

addCommittees(engine, "HSHA",117, "Committee on Administration", "House", "1995-01-04T05:00:00Z", "https://cha.house.gov/", "", 46, "https://cha.house.gov/sites/evo-subsites/republicans-cha.house.gov/files/evo-media-image/2023_house_admin_offical_full_color.png", "https://cha.house.gov/sites/evo-subsites/cha.house.gov/files/styles/evo_image_full_width/public/evo-media-image/rotator-banner-6_0.png?h=0bfb9bfd&itok=TggWu9g-", ["A000370","B001230", "B001261"], ["IBM", "TSLA","NKE"])
addCommittees(engine,"HSAG",117, "Committee on Agriculture", "House", "2014-05-07T08:00:00Z", "https://agriculture.house.gov/", "", 42, "https://agriculture.house.gov/images/logo-2023.svg", "https://democrats-agriculture.house.gov/images/img-01.jpg", "A000370", "IBM")
addCommittees(engine,"HSSO",117, "Committee on Ethics", "House", "1998-04-21T07:00:00Z", "https://ethics.house.gov/", "", 36, "https://ethics.house.gov/sites/ethics.house.gov/files/logo-new_2.png", "https://oce.house.gov/sites/congressionalethics.house.gov/files/styles/congress_home_page_feature_rotator/public/home_page_feature/Capitol-Hill.jpg?itok=4ZOxlUIJ", ["A000370"], ["IBM"])
"""
