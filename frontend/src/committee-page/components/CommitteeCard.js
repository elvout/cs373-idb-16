import { Card, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { formatNumToMoney } from "../../components/Money";

const createMarkup = (html) => {
    return { __html: html };
  };
export default function CommitteeCard(props) {
    const { id, name, chamber, logoUrl, congress, size, investments, year } = props;
    return (
      <Card style={{}}>
        <Card.Img
          variant="top"
          src={logoUrl}
          style={{ height: 200, objectFit: "cover" }}
        />
        <Card.Body>
          <Card.Title>
            <span dangerouslySetInnerHTML={createMarkup(name)}></span>
          </Card.Title>
          <Card.Subtitle>{chamber}</Card.Subtitle>
          <Card.Text></Card.Text>
          <Card.Text> Investments: {formatNumToMoney(investments)} </Card.Text>
          <Card.Text> {`Committee Size: ${size}`}</Card.Text>
          <Card.Text> Start Year: {year} </Card.Text>
          <Card.Text> {`Congress ${congress}`}</Card.Text>
          <LinkContainer to={`/committees/${id}`}>
            <Button variant="primary">{"View"}</Button>
          </LinkContainer>
        </Card.Body>
      </Card>
    );
  }