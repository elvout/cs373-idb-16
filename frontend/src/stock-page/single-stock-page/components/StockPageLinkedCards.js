import { Card, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { getRandomMoney } from "../../../components/Money";

import { getDateString } from "../../utils/date";

export function PoliticianCard(props) {
  const { id, name, imgUrl, party, lastBuy, totalVolume } = props;
  return (
    <Card style={{ margin: 4 }}>
      <Card.Img
        variant="top"
        src={imgUrl}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>{party}</Card.Subtitle>
        <Card.Text>{`Current holdings: ${getRandomMoney(10000)}`}</Card.Text>
        <Card.Text>{`Last Buy: ${getDateString(lastBuy)}`}</Card.Text>
        <LinkContainer to={`/politicians/${id}`}>
          <Button variant="primary">{"View"}</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}

export function CommitteeCard(props) {
  const { id, name, chamber, logoUrl } = props;
  return (
    <Card style={{ width: "18rem", margin: 4 }}>
      <Card.Img
        variant="top"
        src={logoUrl}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>{chamber}</Card.Subtitle>
        <LinkContainer to={`/committees/${id}`}>
          <Button variant="primary">{"View"}</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}
