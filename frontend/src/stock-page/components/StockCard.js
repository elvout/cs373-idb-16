import { Card, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { Link, Router } from "react-router-dom";
import { formatNumToMoney } from "../../components/Money";

export default function StockCard(props) {
  const { symbol, name, price, image, marketCap, sector, industry } = props;
  const createMarkup = (html) => {
    return { __html: html };
  };
  return (
    <Card>
      <Card.Img
        variant="top"
        src={image}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>
          <span dangerouslySetInnerHTML={createMarkup(symbol)}></span>
        </Card.Title>
        <Card.Subtitle>
          <span dangerouslySetInnerHTML={createMarkup(name)}></span>
        </Card.Subtitle>
        <Card.Text>{`Stock Price: $${price}`}</Card.Text>
        <Card.Text>{`Market Cap: ${formatNumToMoney(marketCap)}`}</Card.Text>
        <Card.Text>
          <span
            dangerouslySetInnerHTML={createMarkup(`Sector: ${sector}`)}
          ></span>
        </Card.Text>
        <Card.Text>
          <span
            dangerouslySetInnerHTML={createMarkup(`Industry: ${industry}`)}
          ></span>
        </Card.Text>
        <Link to={`/stocks/${symbol}`}>
          <Button variant="primary">View Stock</Button>
        </Link>
        {/* <LinkContainer to={`/stocks/${symbol}?`}>
          <Button variant="primary">View Stock</Button>
        </LinkContainer> */}
      </Card.Body>
    </Card>
  );
}
