export default function Page(props) {
  const { title, description, graphics } = props;
  return (
    <div>
      <header
        style={{
          width: "100%",
          height: 200,
          backgroundColor: "#D9D9D9",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <h1>{title}</h1>
        <h5>{description}</h5>
      </header>
      {graphics}
    </div>
  );
}
