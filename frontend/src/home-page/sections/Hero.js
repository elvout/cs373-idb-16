import heroImage from "../../images/home-hero.png";
import { Button } from "../../components/Buttons";

export default function Hero() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "end",
      }}
    >
      <div
        style={{
          backgroundImage: `url(${heroImage})`,
          height: "35vw",
          width: "100%",
          backgroundSize: "100% auto",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div
          className="h1"
          style={{
            width: "80%",
            height: 140,
            // backgroundColor: "rgba(255,255,255,0.5)",
            marginTop: '10%',
            position: "relative",
          }}
        >
          <div
            style={{
              marginLeft: 50,
              marginTop: 10,
              fontWeight: "600",
              fontSize: "6.8vw",
              color: "white",
            }}
          >
            {"Capitol Capital"}
          </div>
          <div
            style={{
              marginLeft: 50,
              fontWeight: "500",
              fontSize: "2.5vw",
              color: "white",
            }}
          >
            {"Learn to trade like a politician"}
          </div>
          <div style={{ marginLeft: 50, marginTop: 10 }}>
            <Button label={"Read Docs"} />
          </div>
        </div>
      </div>
    </div>
  );
}
