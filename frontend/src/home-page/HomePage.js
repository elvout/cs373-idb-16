import Hero from "./sections/Hero"
import ModelLinks from "./sections/ModelLinks"
export default function HomePage() {
  return (
    <div style={{ display: "flex", flexFlow: "column", width: "100%", fontFamily: 'Poppins, serif' }}>
      <Hero/>
      <ModelLinks/>
    </div>
  );
}
