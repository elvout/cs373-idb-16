import { useState, useEffect } from "react";
import { StockCard, PoliticianCard } from "./CommitteePageLinkCards";
import { PaginationControl } from "react-bootstrap-pagination-control";

function CommitteePageStockLinks(props) {
  const { stockData } = props;
  const [page, setPage] = useState(1);
  const [cards, setCards] = useState([]);
  const pagination = async (newPage) => {
    const last = newPage * 3;
    const first = last - 3;
    setPage(newPage);

    var temp = [];
    const pageData = stockData.slice(first, last);
    pageData.forEach((obj, i) => {
      temp.push(
        <StockCard
          key={i}
          name={obj.name}
          image={obj.logo_url}
          symbol={obj.stock_id}
        />
      );
    });
    setCards(temp);
  };

  useEffect(() => {
    pagination(1);
  }, []);

  return (
    <div>
      <PaginationControl
        page={page}
        between={3}
        total={stockData.length}
        limit={3}
        changePage={(page) => {
          pagination(page);
        }}
        ellipsis={1}
      />
      <div style={{ display: "flex", flexDirection: "row", marginBottom: 40 }}>
        {cards}
      </div>
    </div>
  );
}
function CommitteePagePoliticianLinks(props) {
  const { politicianData } = props;
  const [page, setPage] = useState(1);
  const [cards, setCards] = useState([]);

  const pagination = async (newPage) => {
    const last = newPage * 3;
    const first = last - 3;
    setPage(newPage);

    var temp = [];
    const pageData = politicianData.slice(first, last);
    pageData.forEach((obj, i) => {
      temp.push(
        <PoliticianCard
          key={i}
          id={obj.politician_id}
          name={obj.name}
          imgUrl={obj.img_url}
          party={obj.party}
          lastBuy={new Date()}
          totalVolume={"123,456,7890"}
        />
      );
    });
    setCards(temp);
  };

  useEffect(() => {
    pagination(1);
  }, []);

  return (
    <div>
      <PaginationControl
        page={page}
        between={3}
        total={politicianData.length}
        limit={3}
        changePage={(page) => {
          pagination(page);
        }}
        ellipsis={1}
      />
      <div style={{ display: "flex", flexDirection: "row", marginBottom: 40 }}>
        {cards}
      </div>
    </div>
  );
}

export default function ModelLinks(props) {
  const { stockData, politicianData } = props;
  return (
    <div>
      <div class="h1">Stocks held</div>
      <CommitteePageStockLinks stockData={stockData} />
      <div class="h1">Members</div>
      <CommitteePagePoliticianLinks politicianData={politicianData} />
    </div>
  );
}
