const apiData = {
    data: [
        // {
        //     name: "QuiverQuant Api",
        //     imageUrl: "https://play-lh.googleusercontent.com/TnAqbtFVHisOkNe0TnmtBuGk8jLIyCh2adAZ3b9K1voeh7G-6UrurM7RPzB_kDz0nWM",
        //     description: "API for Congressional stock transactions",
        //     url: "https://www.quiverquant.com/",
        // },
        // {
        //     name: "Polygon",
        //     imageUrl: "https://polygon.io/images/poly-splash.png",
        //     description: "API for stock information",
        //     url: "https://polygon.io/",
        // },
        {
            name: "AlphaVantage",
            imageUrl: "https://pbs.twimg.com/profile_images/1461739905223213061/uVP4iO8D_400x400.jpg",
            description: "API for stock market data",
            url: "https://www.alphavantage.co/documentation/",
        },
        {
            name: "HouseStockWatcher",
            imageUrl: "https://housestockwatcher.com/static/media/eagle.949767e5.svg",
            description: "API for Congressional stock transactions",
            url: "https://housestockwatcher.com/api",
        },
        {
            name: "OpenSecrets",
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD6BQARHpoLuIAa-feJ8Kw1ss7Pv8BmZoMQXtGtVjbaKR0TilenXaL8u7dyEqL8nj3Yzg&usqp=CAU",
            description: "API for tracking money in politics",
            url: "https://www.opensecrets.org/open-data/api-documentation",
        },
        {
            name: "ProPublica",
            imageUrl: "https://play-lh.googleusercontent.com/8C5GNVMjuD0_jHbIsVH0UqJu_G_mpk_KXtclPkg2ZwnyC9fTfWbWGemUtG_siSXOv2s",
            description: "API for Congress member and committee data",
            url: "https://www.propublica.org/datastore/apis",
        },
        // {
        //     name: "Congress",
        //     imageUrl: "https://www.loc.gov/static/research-centers/law-library-of-congress/images/congressdotgov.png",
        //     description: "API for Congress data",
        //     url: "https://api.congress.gov/",
        // },
        {
            name: "Clearbit API",
            imageUrl: "https://avatars.slack-edge.com/2016-04-13/34462006258_8d4265baa75a2ed9c2ab_512.png",
            description: "Logos provided by Clearbit",
            url: "https://dashboard.clearbit.com/docs#logo-api",
        },
        {
            name: "GitLab API",
            imageUrl: "https://seeklogo.com/images/G/gitlab-logo-FAA48EFD02-seeklogo.com.png",
            description: "API for repository statistics",
            url: "https://docs.gitlab.com/ee/api/rest/",
        },
    ]
}
export default apiData;
