import Committees from "./components/Committees";

export default function CommitteesPage() {
  return (
    <div style={{ marginTop: 10 }}>
      <Committees />
    </div>
  );
}
