import { useState, useEffect } from "react";
import toolchainData from "../data/toolchainData";

function ToolchainCard(props) {
  const { name, imageUrl, description, url } = props;

  const [hover, setHover] = useState(false);

  return (
    <div
      style={{
        height: 400,
        width: 240,
        borderRadius: 10,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: "#FFFFFF",
        marginLeft: 12,
        marginRight: 12,
        marginTop: 30,
        marginBottom: 30,
      }}
      onMouseEnter={() => {
        setHover(true);
      }}
      onMouseLeave={() => {
        setHover(false);
      }}
    >
      <div
        style={{
          width: "90%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <a href={url}>
          <img
            style={{
              width: "100%",
              borderRadius: 10,
              marginTop: 40,
              height: 200,
              objectFit: "contain",
            }}
            src={imageUrl}
          ></img>
        </a>
        <div style={{ fontWeight: 500, fontSize: 24, textAlign: "center" }}>
          {name}
        </div>
        <div style={{ fontWeight: 500, fontSize: 16, textAlign: "center" }}>
          {description}
        </div>
      </div>
    </div>
  );
}

function ToolchainCards() {
  const [cards, setCards] = useState();
  useEffect(() => {
    const tempCards = toolchainData.data.map((object) => {
      return (
        <ToolchainCard
          name={object.name}
          imageUrl={object.imageUrl}
          description={object.description}
          url={object.url}
        />
      );
    });
    setCards(tempCards);
  }, []);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        backgroundColor: "#FAFAFA",
        width: "100%",
        justifyContent: "center",
      }}
    >
      {cards}
    </div>
  );
}

export default function Toolchain() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div style={{ marginTop: 100, fontWeight: 500, fontSize: 60 }}>
        Toolchain
      </div>
      <ToolchainCards />
    </div>
  );
}
