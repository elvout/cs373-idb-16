export default function Header(props) {
  const { imgUrl, party, name, url } = props;
  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            width: 200,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <img
            src={imgUrl}
            style={{ width: 200, height: 200, objectFit: "cover" }}
          />
          <div class="h3">{party}</div>
        </div>
        <div style={{ marginLeft: 50 }}>
          <div class="h1">{name}</div>
          <div style={{ fontSize: 16 }}>
            {`Bio: `}
            <span>
              <a href={url}>{url}</a>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
