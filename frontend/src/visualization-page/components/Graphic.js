export default function Graphic(props) {
  const { title, description, graph } = props;
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        marginTop: 140,
        marginBottom: 140,
      }}
    >
      <h2>{title}</h2>
      {graph}
      <div
        style={{
          width: "90%",
        }}
      >
        {description}
      </div>
    </div>
  );
}
