import { formatNumToMoney } from "../../../components/Money";

export default function Investments(props) {
  const { cash } = props;
  return (
    <div>
      <div className="h3">Investments</div>
      <div>
        <div>{"Cash"}</div>
        <div style={{ fontSize: 50, color: "#278225" }}>{`${formatNumToMoney(cash)}`}</div>
      </div>
    </div>
  );
}
