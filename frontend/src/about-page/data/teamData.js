const teamData = {
  // GitLab commits and issues are calculated using the GitLab API in Team.js
  // alt_names are used to calculate GitLab statistics
  data: [
    {
      name: "Noah Kester",
      role: "Fullstack Dev",
      about:
        "I am a third year CS major at UT Austin. I am from Houston TX. I enjoy Jiu Jitsu, chess, and Civ4",
      profileUrl:
        "https://media.licdn.com/dms/image/C5603AQHt_OK6NFAsnQ/profile-displayphoto-shrink_800_800/0/1644053738527?e=1687392000&v=beta&t=tDnG5-n9Ei4d9ebozVgHv2lM-XU9Ghu-zYsIiJYB110",
      gitlab: {
        username: "noahkester35",
        commits: 0,
        issues: 0,
        unitTests: 0,
      },
      alt_names: new Set(["Noah  Kester"]),
    },
    {
      name: "Jeffery Liu",
      role: "Frontend Dev",
      about:
        "I am a fourth year CS major at UT Austin. I am from Norman Oklahoma. I enjoy reading, listening to podcasts, and playing videogames.",
      profileUrl:
        "https://media.licdn.com/dms/image/D5603AQHJ17hTJzUvcA/profile-displayphoto-shrink_200_200/0/1676282547547?e=1681948800&v=beta&t=yE3U2Fv7767PoME9S8MdwJGCD6wbUZ_o8bUgjSdVQ_I",
      gitlab: {
        username: "jeffreyliu1220",
        commits: 0,
        issues: 0,
        unitTests: 20,
      },
      alt_names: new Set(["MyosotisM2U"]),
    },
    {
      name: "Milaan Dahiya",
      role: "Frontend Dev",
      about:
        "I am a third year CS major at UT Austin from Dallas, TX. I enjoy watching UT sports, playing golf, and playing flight sim.",
      profileUrl: "https://i.ibb.co/CsHfbjw/photo-2023-02-15-22-13-04.jpg",
      gitlab: {
        username: "milaandahiya",
        commits: 0,
        issues: 0,
        unitTests: 20,
      },
      alt_names: new Set(["milaandahiya"]),
    },
    {
      name: "Hyun Park",
      role: "Backend Dev",
      about:
        "I am a fourth year CS major at UT Austin. I am from South Korea not North. I enjoy playing basketball, watching NBA and cooking pasta.",
      profileUrl:
        "https://miro.medium.com/v2/resize:fit:750/format:webp/1*KqR3fXbPiVOM5DFp7728BA.jpeg",
      gitlab: {
        username: "HyunKyu21",
        commits: 0,
        issues: 0,
        unitTests: 5,
      },
      alt_names: new Set(["HyunKyu21", "Hyun"]),
    },
    {
      name: "Elvin Yang",
      role: "Backend Dev",
      about:
        "I am a fourth year CS major at UT Austin. I enjoy hiking, videogames, and working with robots.",
      profileUrl:
        "https://miro.medium.com/v2/resize:fit:840/format:webp/1*AgGphshDI4lxIBeJoBbNmw.jpeg",
      gitlab: {
        username: "elvout",
        commits: 0,
        issues: 0,
        unitTests: 5,
      },
      alt_names: new Set(),
    },
  ],
};
export default teamData;
