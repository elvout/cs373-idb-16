export default function CompanyStats(props) {
  const { sector, industry, address } = props;
  return (
    <div style={{ marginTop: 40 }}>
      <div class="h2">Company Stats:</div>
      <div>{`Sector: ${sector}`}</div>
      <div>{`Industry: ${industry}`}</div>
      <div>{`Address: ${address}`}</div>
    </div>
  );
}
