import { useState, useEffect, useReducer } from "react";
import PoliticiansCard from "../components/PoliticiansCard";
import Spinner from "react-bootstrap/Spinner";
import { PaginationControl } from "react-bootstrap-pagination-control";
import {
  getPoliticianByFilter,
  getPoliticiansInRange,
} from "../utils/politicianApi";
import FilterDropdown from "../../components/FilterDropdown";
import { Button } from "react-bootstrap";
import SearchBar from "../../components/Search";
import { useNavigate, useSearchParams, useLocation } from "react-router-dom";
import CardPage from "../../components/CardPage";

const blankProfilePic =
  "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png";

const ascOptions = ["Order By", "Ascending", "Descending"];
const partyOptions = ["Party", "D", "R"];
const genderOptions = ["Gender", "M", "F"];
const valOptions = ["Sort By", "FirstElected", "CashOnHand", "Debt"];

/*
page (start-end), party, gender, sort order (asc), sort val(...), search

*/

function reducer(state, action) {
  switch (action.type) {
    case "setGender": {
      return { ...state, gender: action.newGender };
    }
    case "setParty": {
      return { ...state, party: action.newParty };
    }
    case "setVal": {
      return { ...state, val: action.newVal };
    }
    case "setAsc": {
      return { ...state, asc: action.newAsc };
    }
    case "setSer": {
      return { ...state, ser: action.newSer };
    }
    case "setPage": {
      return { ...state, page: action.newPage };
    }
  }
}

export default function Politicians() {
  const [searchParams, setSearchParams] = useSearchParams();
  const [cards, setCards] = useState();

  const [filters, dispatch] = useReducer(reducer, {
    page: searchParams.get("start") ? searchParams.get("start") / 20 + 1 : 1,
    gender: searchParams.get("gender")
      ? searchParams.get("gender")
      : genderOptions[0],
    party: searchParams.get("party")
      ? searchParams.get("party")
      : partyOptions[0],
    val: searchParams.get("val") ? searchParams.get("val") : valOptions[0],
    asc: searchParams.get("asc") ? searchParams.get("asc") : ascOptions[0],
    ser: searchParams.get("ser") ? searchParams.get("ser") : "",
  });

  const numPerPage = 20;

  const navigate = useNavigate();
  const location = useLocation();

  const updateCards = (data) => {
    if (!data) {
      return;
    }
    const tempCards = data.map((obj, i) => {
      let name = obj.name;
      let party = obj.party;
      let birth = obj.date_of_birth;

      if (filters.ser !== "") {
        name = name.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
        party = party.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
        birth = birth.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
      }
      return (
        <PoliticiansCard
          key={i}
          name={name}
          party={party}
          holdings={"1234"}
          id={obj.politician_id}
          profileUrl={obj.img_url == null ? blankProfilePic : obj.img_url}
          birthDate={birth}
          firstElected={obj.first_elected}
        />
      );
    });
    setCards(tempCards);
  };
  const updateData = async () => {
    const end = filters.page * numPerPage;
    const start = end - numPerPage;
    const data = await getPoliticianByFilter(
      start,
      end,
      filters.asc,
      filters.party,
      filters.gender,
      filters.val,
      filters.ser
    );
    return data;
  };

  const init = async () => {
    const data = await updateData();
    updateCards(data);
  };
  const onSearch = () => {
    const newQuery = getQueryFromState();
    navigate(newQuery);
  };
  const onChangePage = (page) => {
    dispatch({ type: "setPage", newPage: page });
  };
  const onReset = () => {
    navigate("/politicians");
    window.location.reload(false);
  };

  useEffect(() => {
    onSearch();
  }, [
    filters.page,
    filters.party,
    filters.gender,
    filters.ser,
    filters.val,
    filters.asc,
  ]);

  useEffect(() => {
    init();
  }, [location]);

  useEffect(() => {
    init();
  }, []);

  const getQueryFromState = () => {
    const end = filters.page * numPerPage;
    const start = end - numPerPage;
    var query = `/politicians?start=${start}&end=${end}`;

    if (filters.asc != ascOptions[0]) {
      query += `&asc=${filters.asc == "Ascending" ? 1 : 0}`;
    }
    if (filters.party !== partyOptions[0]) {
      query += `&party=${filters.party}`;
    }
    if (filters.gender !== genderOptions[0]) {
      query += `&gender=${filters.gender}`;
    }
    if (filters.val !== valOptions[0]) {
      query += `&val=${filters.sortVal}`;
    }
    if (filters.ser !== "") {
      query += `&ser=${filters.ser}`;
    }
    return query;
  };

  const filterComponents = [
    <FilterDropdown
      title={filters.asc == 1 ? "Ascending" : "Descending"}
      items={ascOptions}
      onChange={(value) => {
        dispatch({ type: "setAsc", newAsc: value });
      }}
    />,
    <FilterDropdown
      title={filters.party}
      items={partyOptions}
      onChange={(value) => {
        dispatch({ type: "setParty", newParty: value });
      }}
    />,
    <FilterDropdown
      title={filters.gender}
      items={genderOptions}
      onChange={(value) => {
        dispatch({ type: "setGender", newGender: value });
      }}
    />,
    <FilterDropdown
      title={filters.val}
      items={valOptions}
      onChange={(value) => {
        dispatch({ type: "setVal", newVal: value });
      }}
    />,
  ];

  return (
    <CardPage
      search={filters.ser}
      setSearch={(value) => {
        dispatch({ type: "setSer", newSer: value });
      }}
      filterComponents={filterComponents}
      cards={cards}
      reset={onReset}
      page={filters.page}
      numPerPage={numPerPage}
      changePage={onChangePage}
    />
  );
}
