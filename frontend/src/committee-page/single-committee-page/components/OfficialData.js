export default function OfficialData(props) {
  const { chamber, date, size, congress } = props;
  return (
    <div>
      <div class="h1">Official Data:</div>
      <div>{`Chamber: ${chamber}`}</div>
      <div>{`Start date: ${date}`}</div>
      <div>{`Committee Size: ${size}`}</div>
      <div>{`Congress: ${congress}`}</div>
    </div>
  );
}
