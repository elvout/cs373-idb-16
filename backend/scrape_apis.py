import html
import io
import json
import logging
import os
import re
import time
from collections import Counter
from pathlib import Path
from typing import Any, Optional
from urllib.parse import unquote

import requests
from bs4 import BeautifulSoup
from PIL import Image
from seleniumwire import webdriver

RAW_RESPONSES_DIR = Path("raw-api-responses")


class CongressGovApi:
    @classmethod
    def _base_url(cls) -> str:
        return "https://api.congress.gov/v3/"

    @classmethod
    def _base_params(cls) -> dict[str, str]:
        return {"api_key": "eXGRKaupaPHPJ3lTZcog8dwrMcVApnx1vvzFhydS"}

    @classmethod
    def all_members(cls) -> None:
        # The list is paginated, so we need to merge the results of multiple
        # requests. The max query size is 250.
        merged_responses: list[dict[str, Any]] = []

        url = cls._base_url() + "member?limit=250&offset=0"
        while True:
            logging.info(url)
            response = requests.get(url, params=cls._base_params())
            json_body = response.json()
            # compress extra levels of encapsulation
            for obj in json_body["members"]:
                merged_responses.append(obj["member"])
            try:
                url = json_body["pagination"]["next"]
            except KeyError:
                break

        save_path = RAW_RESPONSES_DIR / cls.__name__ / "members.json"
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(merged_responses, f, indent=4)

    @classmethod
    def all_committees(cls) -> None:
        # The list is paginated, so we need to merge the results of multiple
        # requests. The max query size is 250.
        merged_responses: list[dict[str, Any]] = []

        url = cls._base_url() + "committee?limit=250&offset=0"
        while True:
            logging.info(url)
            response = requests.get(url, params=cls._base_params())
            json_body = response.json()
            merged_responses.extend(json_body["committees"])
            try:
                url = json_body["pagination"]["next"]
            except KeyError:
                break

        save_path = RAW_RESPONSES_DIR / cls.__name__ / "committees.json"
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(merged_responses, f, indent=4)

        for committee in merged_responses:
            if committee["systemCode"].endswith("00"):
                cls.single_committee(committee["chamber"], committee["systemCode"])

    @classmethod
    def single_committee(cls, chamber: str, code: str) -> None:
        url = cls._base_url() + f"committee/{chamber}/{code}"
        logging.info(url)
        response = requests.get(url, params=cls._base_params())
        json_body = response.json()["committee"]
        save_path = RAW_RESPONSES_DIR / cls.__name__ / f"{chamber}/{code}.json"
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(json_body, f, indent=4)


class ProPublicaCongressApi:
    @classmethod
    def _base_url(cls) -> str:
        return "https://api.propublica.org/congress/v1/"

    @classmethod
    def _base_headers(cls) -> dict[str, str]:
        return {"X-API-Key": "YTAScgvfuaktzgeIkAY4WSZoDI9vzkiyBxvEBvU7"}

    @classmethod
    def all_members(cls, index: int = 117) -> None:
        for chamber in ["house", "senate"]:
            url = cls._base_url() + f"{index}/{chamber}/members.json"
            logging.info(url)
            response = requests.get(url, headers=cls._base_headers())

            save_path = (
                RAW_RESPONSES_DIR / cls.__name__ / f"{index}/{chamber}/members.json"
            )
            os.makedirs(save_path.parent, exist_ok=True)
            with open(save_path, "w") as f:
                json.dump(response.json(), f, indent=4)

    @classmethod
    def all_committees(cls, index: int = 117) -> None:
        for chamber in ["house", "senate", "joint"]:
            url = cls._base_url() + f"{index}/{chamber}/committees.json"
            logging.info(url)
            response = requests.get(url, headers=cls._base_headers())
            json_body = response.json()

            save_path = (
                RAW_RESPONSES_DIR / cls.__name__ / f"{index}/{chamber}/committees.json"
            )
            os.makedirs(save_path.parent, exist_ok=True)
            with open(save_path, "w") as f:
                json.dump(json_body, f, indent=4)

            for result in json_body["results"]:
                for committee in result["committees"]:
                    committee_id = committee["id"]
                    committee_api_url = committee["api_uri"]
                    logging.info(committee_api_url)
                    response = requests.get(
                        committee_api_url, headers=cls._base_headers()
                    )
                    save_path = (
                        RAW_RESPONSES_DIR
                        / cls.__name__
                        / f"{index}/{chamber}/committees/{committee_id}.json"
                    )
                    os.makedirs(save_path.parent, exist_ok=True)
                    with open(save_path, "w") as f:
                        json.dump(response.json(), f, indent=4)


class HouseStockWatcherApi:
    @classmethod
    def all_transactions_starting_jan_2020(cls) -> Counter[str]:
        url = "https://house-stock-watcher-data.s3-us-west-2.amazonaws.com/data/all_transactions.json"
        logging.info(url)
        response = requests.get(url)

        save_path = RAW_RESPONSES_DIR / cls.__name__ / "all_transactions.json"
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(response.json(), f, indent=4)

        ticker_counter: Counter[str] = Counter()
        for tx in response.json():
            ticker_counter[tx["ticker"]] += 1
        return ticker_counter


class SenateStockWatcherApi:
    @classmethod
    def all_transactions_starting_jan_2020(cls) -> Counter[str]:
        url = "https://senate-stock-watcher-data.s3-us-west-2.amazonaws.com/aggregate/all_transactions.json"
        logging.info(url)
        response = requests.get(url)

        save_path = RAW_RESPONSES_DIR / cls.__name__ / "all_transactions.json"
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(response.json(), f, indent=4)

        ticker_counter: Counter[str] = Counter()
        for tx in response.json():
            ticker_counter[tx["ticker"]] += 1
        return ticker_counter


class AlphaVantageApi:
    @classmethod
    def _base_url(cls) -> str:
        return "https://www.alphavantage.co/"

    @classmethod
    def _base_params(cls) -> dict[str, str]:
        return {"apikey": "3CAYF6AGZDGLDVJ0"}

    @classmethod
    def overview(cls, ticker: str) -> None:
        save_path = RAW_RESPONSES_DIR / cls.__name__ / "overviews.json"
        # overviews we have already
        current = {}
        if os.path.exists(save_path):
            with open(save_path, "r") as f:
                current = json.load(f)

        if ticker in current:
            logging.info(f"{ticker}: overview cache hit")
            return

        url = cls._base_url() + f"query?function=OVERVIEW&symbol={ticker}"
        logging.info(url)
        while ticker not in current:
            response = requests.get(url, params=cls._base_params())
            json_body = response.json()
            if len(json_body) == 0:
                current[ticker] = None
            elif "Note" in json_body:
                logging.warning("Rate limit exceeded; retrying after 12 seconds")
                time.sleep(12)
            else:
                current[ticker] = json_body

        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(current, f, indent=4, sort_keys=True)

        time.sleep(60 / 5)


class TwelveDataApi:
    @classmethod
    def _base_url(cls) -> str:
        return "https://api.twelvedata.com/"

    @classmethod
    def _base_params(cls) -> dict[str, str]:
        return {"apikey": "28e7108ae67347eca001c9d2a1dea740"}

    @classmethod
    def logo(cls, ticker: str) -> None:
        save_path = RAW_RESPONSES_DIR / cls.__name__ / "logos.json"
        # logos we have already
        current_logos = {}
        if os.path.exists(save_path):
            with open(save_path, "r") as f:
                current_logos = json.load(f)

        if ticker in current_logos:
            logging.info(f"{ticker}: logo cache hit")
            return

        url = cls._base_url() + f"logo?symbol={ticker}"
        logging.info(url)
        while ticker not in current_logos:
            response = requests.get(url, params=cls._base_params())
            json_body = response.json()
            if "code" in json_body:
                if json_body["code"] == 404:
                    current_logos[ticker] = None
                elif json_body["code"] == 429:
                    logging.warning("Rate limit exceeded; retrying after 10 seconds")
                    time.sleep(10)
            else:
                current_logos[ticker] = json_body["url"]

        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(current_logos, f, indent=4, sort_keys=True)

        time.sleep(60 / 8)


class NitterApi:
    @classmethod
    def _base_url(cls) -> str:
        return "https://nitter.net/"

    @classmethod
    def profile_images(cls, handle: str) -> dict[str, Optional[str]]:
        handle = handle.replace("@", "")

        save_path = RAW_RESPONSES_DIR / cls.__name__ / "profiles.json"
        current_profiles = {}
        if os.path.exists(save_path):
            with open(save_path, "r") as f:
                current_profiles = json.load(f)

        if handle in current_profiles:
            return current_profiles[handle]

        current_profiles[handle] = {"avatar": None, "banner": None}

        url = f"https://nitter.net/{handle}"
        logging.info(url)
        response = requests.get(url)
        if response.status_code >= 400:
            logging.warning(f"Twitter handle '{handle}' not found")
        else:
            soup = BeautifulSoup(response.text, "html.parser")
            avatar_element = soup.find("a", {"class": "profile-card-avatar"})
            banner_element = soup.find("div", {"class": "profile-banner"})

            if avatar_element is not None and avatar_element.has_attr("href"):  # type: ignore
                current_profiles[handle]["avatar"] = "https://" + unquote(avatar_element["href"][5:])  # type: ignore

            if banner_element is not None and banner_element.a.has_attr("href"):  # type: ignore
                current_profiles[handle]["banner"] = unquote(banner_element.a["href"][5:])  # type: ignore

        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(current_profiles, f, indent=4, sort_keys=True)
        return current_profiles[handle]


class ParseWebsiteData:
    twitter_pattern = re.compile(
        r'(https?://)(www.)?(twitter.com/)(#!/)?(intent/follow.+name=)?(\w+?)(?=[?/"])'
    )

    @classmethod
    def _headers(cls) -> "dict[str, Any]":
        return {
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36"
        }

    @classmethod
    def _find_twitter_handle(cls, text: str) -> Optional[str]:
        matches = re.findall(cls.twitter_pattern, html.unescape(text))
        matches = [match for match in matches if "share" not in match]
        matches = [match for match in matches if "intent" not in match]
        if matches:
            return matches[0][-1]
        return None

    @classmethod
    def images(cls, url: str) -> dict[str, Optional[str]]:
        """
        Scrape a website for a banner image by:

        1. First checking if there's a twitter link in the raw html.
            a. Twitter accounts typically have good banner images
            b. We'll also use the profile image logo since it tends to be higher quality.
        2. Check if there's a twitter link in the "/about" url.
        3. Interact with the website using Selenium
            a. Check again for a twitter account
            b. Sort the images on the website by width and entropy
        """
        save_path = RAW_RESPONSES_DIR / cls.__name__ / "websites.json"
        current_websites = {}
        if os.path.exists(save_path):
            with open(save_path, "r") as f:
                current_websites = json.load(f)

        if url in current_websites:
            return current_websites[url]

        current_websites[url] = {"banner": None, "twitter_account": None}

        logging.info(url)

        # edge case, twitter itself
        twitter_handle = "twitter" if "twitter" in url else None
        if not twitter_handle:
            try:
                twitter_handle = cls._find_twitter_handle(
                    requests.get(url, timeout=10, headers=cls._headers()).text
                )
            except BaseException:
                pass
        if not twitter_handle:
            try:
                twitter_handle = cls._find_twitter_handle(
                    requests.get(
                        url + "/about", timeout=10, headers=cls._headers()
                    ).text
                )
            except BaseException:
                pass

        # Do a check beforehand because we need to see if the twitter handle has
        # a banner image
        if twitter_handle:
            current_websites[url]["twitter_account"] = twitter_handle
            current_websites[url] |= NitterApi.profile_images(twitter_handle)

        possible_images = []
        if not twitter_handle or not current_websites[url]["banner"]:
            driver = webdriver.Chrome()
            driver.set_page_load_timeout(15)
            driver.get(url)

            # some websites close immediately
            time.sleep(5)

            twitter_handle = cls._find_twitter_handle(driver.page_source)
            for request in driver.requests:
                if (
                    request.response is not None
                    and request.response.status_code < 400
                    and "Content-Type" in request.response.headers
                    and request.response.headers["Content-Type"].startswith("image")
                    and (
                        request.response.headers["Content-Type"].endswith("jpeg")
                        or request.response.headers["Content-Type"].endswith("png")
                        or request.response.headers["Content-Type"].endswith("webp")
                    )
                ):
                    if "sprite" in request.url or "icon" in request.url:
                        continue

                    with io.BytesIO(request.response.body) as fp:
                        try:
                            img = Image.open(fp)
                            possible_images.append(
                                {
                                    "url": request.url,
                                    "content-length": len(request.response.body),
                                    "size": img.size,
                                    "entropy": img.entropy(),
                                }
                            )
                        except BaseException:
                            pass
            # Sort first by the width of the image, then the entropy of the
            # image, which acts as a crude measurement of the complexity of the
            # image to avoid e.g. blank images
            possible_images.sort(key=lambda d: (d["size"][0], d["entropy"]))

        if twitter_handle:
            current_websites[url]["twitter_account"] = twitter_handle
            current_websites[url] |= NitterApi.profile_images(twitter_handle)
        if not current_websites[url]["banner"] and possible_images:
            current_websites[url]["banner"] = possible_images[-1]["url"]

        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            json.dump(current_websites, f, indent=4, sort_keys=True)
        return current_websites[url]


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    # CongressGovApi.all_members()
    # CongressGovApi.all_committees()
    # ProPublicaCongressApi.all_members()
    # ProPublicaCongressApi.all_committees()
    # tickers_house = HouseStockWatcherApi.all_transactions_starting_jan_2020()
    # tickers_senate = SenateStockWatcherApi.all_transactions_starting_jan_2020()

    # tickers_all = tickers_house + tickers_senate

    # These APIs are rate-limited, so you can't run them all the time.
    # for i, (ticker, _) in enumerate(tickers_all.most_common()):
    #     logging.info(f"{i}/{len(tickers_all)}")
    #     TwelveDataApi.logo(ticker.strip())
    #     AlphaVantageApi.overview(ticker.strip())

    # import seleniumwire.backend
    # import seleniumwire.handler
    # import seleniumwire.storage

    # seleniumwire.backend.log.setLevel(logging.WARNING)
    # seleniumwire.storage.log.setLevel(logging.WARNING)
    # seleniumwire.handler.log.setLevel(logging.WARNING)
    # with open(RAW_RESPONSES_DIR / "TwelveDataApi/logos.json", "r") as f:
    #     logos = json.load(f)
    #     for url in logos.values():
    #         if not url:
    #             continue
    #         website_url: str = url.split("/")[-1]
    #         if website_url.count(".") == 1:
    #             website_url = "https://www." + website_url
    #         else:
    #             website_url = "https://" + website_url

    #         ParseWebsiteData.images(website_url)

    # handle = "Expedia"
    # d = NitterApi.profile_images(handle) | {"twitter_account": handle}
    # print(str(d).replace("'", '"'))
