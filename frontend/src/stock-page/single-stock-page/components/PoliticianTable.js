import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

import { formatDate } from "../../../components/Time";
import { getRandomMoney } from "../../../components/Money";

export default function PoliticianTable(props) {
  const { politicianData } = props;
  const [rows, setRows] = useState([]);

  const initRows = () => {
    var tempRows = [];
    politicianData.forEach((obj) => {
      tempRows.push(
        <tr>
          <Link to={`/politicians/${obj.id}`}>
            <td>{obj.name}</td>
          </Link>
          <td>{getRandomMoney(10000)}</td>
          <td>{`${formatDate(new Date())}`}</td>
        </tr>
      );
    });
    setRows(tempRows);
  };

  useEffect(() => {
    initRows();
  }, []);

  return (
    <div style={{ height: 400, overflowY: "scroll" }}>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Current Holdings</th>
            <th>Last Buy</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </div>
  );
}
