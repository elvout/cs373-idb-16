import { Button } from "../../../components/Buttons";

import { PaginationControl } from "react-bootstrap-pagination-control";

import { useState, useEffect } from "react";

import PoliticianTable from "./PoliticianTable";
import { PoliticianCard, CommitteeCard } from "./StockPageLinkedCards";

function StockPagePoliticianLinks(props) {
  const { politicianData } = props;
  const [page, setPage] = useState(1);
  const [cards, setCards] = useState([]);

  const pagination = async (newPage) => {
    const last = newPage * 3;
    const first = last - 3;
    setPage(newPage);

    var temp = [];
    const pageData = politicianData.slice(first, last);
    pageData.forEach((obj, i) => {
      temp.push(
        <PoliticianCard
          key={i}
          id={obj.politician_id}
          name={obj.name}
          imgUrl={obj.img_url}
          party={obj.party}
          lastBuy={new Date()}
          totalVolume={"1234"}
        />
      );
    });
    setCards(temp);
  };

  useEffect(() => {
    pagination(1);
  }, []);

  return (
    <div>
      <PaginationControl
        page={page}
        between={3}
        total={politicianData.length}
        limit={3}
        changePage={(page) => {
          pagination(page);
        }}
        ellipsis={1}
      />
      <div style={{ display: "flex", flexDirection: "row", marginBottom: 40 }}>
        {cards}
      </div>
      <PoliticianTable politicianData={politicianData} />
    </div>
  );
}

function StockPageCommitteeLinks(props) {
  const { committeeData } = props;
  const [page, setPage] = useState(1);
  const [cards, setCards] = useState([]);
  const pagination = async (newPage) => {
    const last = newPage * 3;
    const first = last - 3;
    setPage(newPage);

    var temp = [];
    const pageData = committeeData.slice(first, last);
    pageData.forEach((obj, i) => {
      temp.push(
        <CommitteeCard
          key={i}
          id={obj.committee_id}
          name={obj.name}
          chamber={obj.chamber}
          logoUrl={obj.logo_url}
        />
      );
    });
    setCards(temp);
  };

  useEffect(() => {
    pagination(1);
  }, []);

  return (
    <div>
      <PaginationControl
        page={page}
        between={3}
        total={committeeData.length}
        limit={3}
        changePage={(page) => {
          pagination(page);
        }}
        ellipsis={1}
      />
      <div style={{ display: "flex", flexDirection: "row", marginBottom: 40 }}>
        {cards}
      </div>
    </div>
  );
}

export default function ModelLinks(props) {
  const { politicianData, committeeData } = props;

  return (
    <div style={{ marginTop: 40 }}>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <div class="h2">Top Political Buyers</div>
        {/* <Button label={"Table View"} /> */}
      </div>
      <StockPagePoliticianLinks politicianData={politicianData} />
      <div class="h2">Top Committees</div>
      <StockPageCommitteeLinks committeeData={committeeData} />
    </div>
  );
}
