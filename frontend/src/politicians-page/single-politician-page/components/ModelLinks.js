import { StockModelCard, CommitteeCard } from "./PoliticianPageLinkedCards";
import { useState, useEffect } from "react";
import { PaginationControl } from "react-bootstrap-pagination-control";

function PoliticianPageCommitteeLinks(props) {
  const { committeeData } = props;
  const [page, setPage] = useState(1);
  const [cards, setCards] = useState([]);
  const pagination = async (newPage) => {
    const last = newPage * 3;
    const first = last - 3;
    setPage(newPage);

    var temp = [];
    const pageData = committeeData.slice(first, last);
    pageData.forEach((obj, i) => {
      temp.push(
        <CommitteeCard
          key={i}
          id={obj.committee_id}
          name={obj.name}
          chamber={obj.chamber}
          logoUrl={obj.logo_url}
        />
      );
    });
    setCards(temp);
  };

  useEffect(() => {
    pagination(1);
  }, []);

  return (
    <div>
      <PaginationControl
        page={page}
        between={3}
        total={committeeData.length}
        limit={3}
        changePage={(page) => {
          pagination(page);
        }}
        ellipsis={1}
      />
      {cards.length == 0 ? (
        <div
          className="h3"
          style={{ marginTop: 20, marginBottom: 20 }}
        >{`No committee membership for this politician`}</div>
      ) : (
        <div
          style={{ display: "flex", flexDirection: "row", marginBottom: 40 }}
        >
          {cards}
        </div>
      )}
    </div>
  );
}

function PoliticianPageStockLinks(props) {
  const { stockData } = props;
  const [page, setPage] = useState(1);
  const [cards, setCards] = useState([]);
  const pagination = async (newPage) => {
    const last = newPage * 3;
    const first = last - 3;
    setPage(newPage);

    var temp = [];
    const pageData = stockData.slice(first, last);
    pageData.forEach((obj, i) => {
      temp.push(
        <StockModelCard
          key={i}
          name={obj.name}
          logoUrl={obj.logo_url}
          symbol={obj.stock_id}
        />
      );
    });
    setCards(temp);
  };

  useEffect(() => {
    pagination(1);
  }, []);

  return (
    <div>
      <PaginationControl
        page={page}
        between={3}
        total={stockData.length}
        limit={3}
        changePage={(page) => {
          pagination(page);
        }}
        ellipsis={1}
      />
      {cards.length == 0 ? (
        <div
          className="h3"
          style={{ marginTop: 20, marginBottom: 20 }}
        >{`No Stock Records for this politician`}</div>
      ) : (
        <div
          style={{ display: "flex", flexDirection: "row", marginBottom: 40 }}
        >
          {cards}
        </div>
      )}
    </div>
  );
}

export default function ModelLinks(props) {
  const { stockData, committeeData } = props;
  return (
    <div>
      <div class="h1">Top Stocks</div>
      <PoliticianPageStockLinks stockData={stockData} />
      <div class="h1">Top Committees</div>
      <PoliticianPageCommitteeLinks committeeData={committeeData} />
    </div>
  );
}
