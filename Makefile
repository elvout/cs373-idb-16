# Basic Makefile Reference
#
# .PHONY: target_name
#   Tells make to ignore a file called "target_name" if it exists, and always
#   run the specified target.
#	Reference: https://www.gnu.org/software/make/manual/html_node/Phony-Targets.html
#
# target_name:
#     -command1
#     command2
#   The hypen in front of "command1" tells make to continue running subsequent
#   commands even if command1 fails with nonzero exit code.

BACKEND_PORT := 80
BACKEND_IMAGE_NAME := idb-backend
FRONTEND_PORT := 443
FRONTEND_IMAGE_NAME := idb-frontend

default:
	@echo Please specify one of the following targets:
# Print a list of all make targets: https://stackoverflow.com/a/26339924
	@LC_ALL=C $(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/(^|\n)# Files(\n|$$)/,/(^|\n)# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

# Build the docker image for the frontend.
.PHONY: build-frontend
build-frontend:
	docker build \
		--tag ${FRONTEND_IMAGE_NAME} \
		frontend/

# Build the docker image for the backend.
.PHONY: build-backend
build-backend:
	docker build \
		--tag ${BACKEND_IMAGE_NAME} \
		backend/

# Launch both the frontend and backend in docker containers.
.PHONY: launch-full
launch-full: build-frontend build-backend
	sudo docker compose up --detach --timeout 1

# Run backend tests in a docker container.
.PHONY: run-backend-tests
run-backend-tests: build-backend
	docker run \
		--rm \
		--interactive \
		--tty \
		${BACKEND_IMAGE_NAME} \
		mypy .

	docker run \
		--rm \
		--interactive \
		--tty \
		${BACKEND_IMAGE_NAME} \
		flake8 .
