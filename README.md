# CapitolCapital

## Group Info

Group #16

### Team Members

| Name          | GitLab ID       | EID      |
| ---           | ---             | ---      |
| Noah Kester   | @noahkester35   | ngk333   |
| Milaan Dahiya | @milaandahiya   | mkd2332  |
| Jeffrey Liu   | @jeffreyliu1220 | jzl267   |
| Hyun Park     | @HyunKyu21      | hkp376   |
| Elvin Yang    | @elvout         | eyy92    |

### Project Leaders

| Phase | Project Leader |
| ---   | ---            |
| 1     | Noah Kester    |
| 2     | Elvin Yang     |
| 3     | Jeffrey Liu    |
| 4     | Milaan Dahiya  |

## Git SHA

| Phase | Git SHA    |
| ---   | ---        |
| 1     | 61a78ef51d |
| 2     | 273827d1c4 |
| 3     | cc0debb4fd |
| 4     | 5bf33761cc |

## Link to GitLab Pipelines

[GitLab Pipelines](https://gitlab.com/elvout/cs373-idb-16/-/pipelines)

## Link to Website

[capitolcapital.us](https://capitolcapital.us)

## Completion Times

### Phase 1

| Name          | Estimated | Actual |
| ---           | ---       | ---    |
| Noah Kester   | 12        | 15     |
| Milaan Dahiya | 12        | 15     |
| Jeffrey Liu   | 12        | 15     |
| Hyun Park     | 12        | 15     |
| Elvin Yang    | 12        | 15     |

### Phase 2

| Name          | Estimated | Actual |
| ---           | ---       | ---    |
| Noah Kester   | 20        | 30     |
| Milaan Dahiya | 20        | 33     |
| Jeffrey Liu   | 20        | 30     |
| Hyun Park     | 30        | 33     |
| Elvin Yang    | 30        | 36     |

### Phase 3

| Name          | Estimated | Actual |
| ---           | ---       | ---    |
| Noah Kester   | 20        | 26     |
| Milaan Dahiya | 20        | 23     |
| Jeffrey Liu   | 20        | 24     |
| Hyun Park     | 20        | 33     |
| Elvin Yang    | 20        | 22     |

### Phase 4

| Name          | Estimated | Actual |
| ---           | ---       | ---    |
| Noah Kester   | 15        | 20     |
| Milaan Dahiya | 15        | 20     |
| Jeffrey Liu   | 15        | 20     |
| Hyun Park     | 10        | 12     |
| Elvin Yang    | 10        | 8      |


## Project Description

Website Name: [capitolcapital.us](https://capitolcapital.us)

Project Proposal: CapitolCapital will be a website that allows users to look at trading information for members of the US Congress. It will provide individual information about congress members, public companies, and congressional committees, as well as connections between these models, allowing people to track trends in political financing and investing.

### Main Data APIs

- Stock Market API: <https://www.alphavantage.co>
- Stock Transactions API: <https://housestockwatcher.com/api>
- Campaign Funding Information: <https://www.opensecrets.org/open-data/api>
- Congressional Data (Members, Committees): <https://www.propublica.org/datastore/apis>

[//]: # "These are comments"
[//]: # "- Stock Market API: <https://polygon.io>"
[//]: # "- Trading Data: <https://api.quiverquant.com/>"
[//]: # "- Additional Congressional Data (Members, Committees): <https://api.congress.gov/#/>"

### Models

#### Politicians

- Estimated Instance Count: ~500
- Attributes for filtering and sorting:
  - Name
  - Age
  - Years in Government
  - Political Party
  - Net Worth
- Media and Total Attributes:
  - Image
  - Short Bio
  - Represented District/State (with Map)
  - Twitter feed (if applicable)
  - Link to Government website
  - Campaign Funding Details
- Connection to Other Models
  - Which stocks/companies has this individual invested in?
  - Which committees are this member in?

#### Stocks / Companies

- Estimated Instance Count: ~500
- Attributes for filtering and sorting:
  - Name of company
  - Stock symbol
  - Company Value
  - Current Stock Price
  - Company Age
- Media and total attributes:
  - Company Website
  - Company Logo
  - Notable Leadership
  - Company twitter (if applicable)
  - Company Headquarters Location (With Map)
- Connection to Other Models:
  - Which politicians invest in this company?
  - Which committees/political parties tend to invest in this company?

#### Committees / Political Parties

- Estimated Instance Count: ~250
- Attributes for filtering and sorting
  - Name of committee
  - Overall returns
  - Range of Tenure (specific instances of the committees)
  - Politician Names (membership)
  - Categories (house, senate, defense, etc)
  - Size
- Media and total attributes:
  - Official government website
  - Logo/Seal
  - Twitter feed (if applicable)
- Connection to other models:
  - Politician membership
  - Aggregate investment in stocks/companies

### Organization Technique

We plan on tentatively using the same organization technique used in previous years. Each model with have one page with a grid of cards. Each card expends to a more detailed page with connections to cards in other models.

### Questions

1. Which stocks have my representative invested in?
2. Which politicians have invested in company XYZ?
3. Which political parties support specific companies?
