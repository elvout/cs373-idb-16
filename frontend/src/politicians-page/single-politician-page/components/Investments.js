export default function Investments(props) {
  const { cash, debt } = props;
  return (
    <div style={{ marginTop: 60 }}>
      <div className="h3">Investments</div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div>
          <div className="h5">{"Cash"}</div>
          <div style={{ fontSize: 50, color: "#278225" }}>{cash}</div>
        </div>
        <div style={{ marginLeft: 200 }}>
          <div className="h5">{"Debt"}</div>
          <div style={{ fontSize: 50, color: "#EA5D5D" }}>{debt}</div>
        </div>
      </div>
    </div>
  );
}
