import Filtering from "./sections/Filtering";
import Stocks from "./sections/Stocks";

export default function StockPage() {
  return (
    <div>
      <Stocks />
    </div>
  );
}
