import { Card, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

export function StockModelCard(props) {
  const { symbol, name, logoUrl } = props;
  return (
    <Card style={{ width: "18rem", margin: 4 }}>
      <Card.Img
        variant="top"
        src={logoUrl}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>{symbol}</Card.Title>
        <Card.Subtitle>{name}</Card.Subtitle>
        <LinkContainer to={`/stocks/${symbol}`}>
          <Button variant="primary">{"View"}</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}
export function CommitteeCard(props) {
  const { id, name, chamber, logoUrl } = props;
  return (
    <Card style={{ width: "18rem", margin: 4 }}>
      <Card.Img
        variant="top"
        src={logoUrl}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>{chamber}</Card.Subtitle>
        <LinkContainer to={`/committees/${id}`}>
          <Button variant="primary">{"View"}</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}
