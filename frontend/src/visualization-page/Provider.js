import Page from "./components/Page";
import Graphic from "./components/Graphic";
import PBarChart from "./components/PBarChart";
import PScatterChart from "./components/PScatterChart";
import { useEffect, useState } from "react";
import { getRentPrices, getCityJobs, getTuition } from "./utils/visualsApi";
import PPieChart from "./components/PPieChart";
export default function Provider(props) {
  const [data1, setData1] = useState([]);
  const [data2, setData2] = useState([]);
  const [data3, setData3] = useState([]);
  const getData = async () => {
    const temp1 = await getRentPrices();
    setData1(temp1);
    const temp2 = await getCityJobs();
    setData2(temp2);
    const temp3 = await getTuition();
    setData3(temp3);
  };

  useEffect(() => {
    getData();
  }, []);

  const graphics = (
    <div style={{ width: "100%" }}>
      <Graphic
        title={"Rent prices"}
        description={
          "We were surprised to see that there were renters that could pay over 10,000+ a month for rent instead of just buying a home. Because all of the data was geared to new grads and college students we expected there to be a much larger right skew to the data and more affordable prices."
        }
        graph={<PBarChart data={data1} />}
      />
      <Graphic
        title={"Jobs per city"}
        description={
          "Based on the data we were initially really confused on the randomness of cities compared to what we expected. LA and NY are not even listed and there seems to be a lot of small cities that have a large amount of jobs. There also was a huge portion of the data that was based in Houston. We think that these discrepencies in the data is caused by the APIs that were used"
        }
        graph={<PPieChart data={data2} />}
      />
      <Graphic
        title={"In-state vs out-of-state tuition"}
        description={
          "The line in the center of the data makes sense because there are a lot of schools that offer the same price for in-state and out-of-state tuition. It also make sense that schools try to encourage in-state by always making the out-of-state tuition higher or equal and never less than. We think that the line has a significant amount of private schools which is why the most expensive schools all fall on this line."
        }
        graph={<PScatterChart data={data3} />}
      />
    </div>
  );

  return (
    <Page
      title={"Provider Visualizations"}
      description={
        "Visualize the collected data from Futur Findr for living your best life"
      }
      graphics={graphics}
    />
  );
}
