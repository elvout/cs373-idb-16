const republicanUrl =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Republicanlogo.svg/2359px-Republicanlogo.svg.png";
const democratUrl =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/DemocraticLogo.svg/800px-DemocraticLogo.svg.png";

export default function OfficialData(props) {
  const { party, gender, dob, elected } = props;
  return (
    <div>
      <div class="h1">Official Data:</div>
      <div>{`Party: ${party}`}</div>
      <img
        style={{ objectFit: "cover", height: 100, width: 100 }}
        src={party == "R" ? republicanUrl : democratUrl} //TODO FIX FOR INDEPENDENTS/GREEN/LIBS/ETC
      />
      <div>{`Gender: ${gender}`}</div>
      <div>{`Date of Birth: ${dob}`}</div>
      <div>{`First Elected: ${elected}`}</div>
    </div>
  );
}
