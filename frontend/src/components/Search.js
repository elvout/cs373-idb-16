import { FaSearch } from "react-icons/fa";

export default function SearchBar(props) {
  const { search, setSearch } = props;
  return (
    <div
      style={{
        height: 40,
        width: "100%",
        margin: 10,
      }}
      className="flex-center"
    >
      <div
        className="flex-center"
        style={{ height: "100%", width: 40, backgroundColor: "#3C8BE2" }}
      >
        <FaSearch color="#FFFFFF" size={"50%"} />
      </div>
      <input
        style={{ height: "100%", width: "60%" }}
        value={search}
        onChange={(event) => {
          setSearch(event.target.value);
        }}
      />
    </div>
  );
}
