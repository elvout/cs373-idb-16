import { useLocation } from "react-router-dom";
import { useState, useEffect } from "react";
import Header from "./components/Header";
import Investments from "./components/Investments";
import OfficialData from "./components/OfficialData";
import ModelLinks from "./components/ModelLinks";
import Spinner from "react-bootstrap/Spinner";
import { getPoliticianDetailsById } from "../utils/politicianApi";
import {
  getStockDetailsById,
  getStocksDetailsByIdList,
} from "../../stock-page/utils/stockApi";
import { getCommitteeDetailsByIdList } from "../../committee-page/utils/committeeApi";
import { getRandomMoney } from "../../components/Money";
import { TwitterTimelineEmbed } from 'react-twitter-embed';


export default function SinglePoliticianPage() {
  const [data, setData] = useState();
  const [stockData, setStockData] = useState();
  const [committeeData, setCommitteeData] = useState();

  const location = useLocation();
  const id = location.pathname.split("/")[2];

  const updateData = async () => {
    const tempPoliticianData = await getPoliticianDetailsById(id);
    const tempStockData = await getStocksDetailsByIdList(
      tempPoliticianData.stocks_traded
    );
    const tempCommitteeData = await getCommitteeDetailsByIdList(
      tempPoliticianData.committees_membership
    );
    setData(tempPoliticianData);
    setStockData(tempStockData);
    setCommitteeData(tempCommitteeData);
  };
  useEffect(() => {
    updateData();
  }, []);

  if (!data || !stockData || !committeeData) {
    return (
      <div
        style={{
          width: "100%",
          height: "100vh",
          display: "flex",
          flexDirection: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    );
  }

  return (
    <div style={{ padding: "4%" }}>
      <Header
        imgUrl={data.img_url}
        party={data.party}
        name={data.name}
        url={data.url}
      />
      <Investments cash={getRandomMoney(200000)} debt={getRandomMoney(10000)} />
      <img
        src={
          data.banner_url != null ? data.banner_url : "https://www.chainbridgebank.com/assets/content/Jml2O9xo/Political%20Committee%20-1154438278.jpg"
        }
        style={{
          width: "100%",
          height: 300,
          objectFit: "cover",
        }}
      />
      <OfficialData
        party={data.party}
        gender={data.gender}
        dob={data.date_of_birth}
        elected={data.first_elected}
      />
      {data.twitter_account == null ? <div/> :
        <TwitterTimelineEmbed
          sourceType="profile"
          screenName={data.twitter_account.substring(0, 1) == "@" ? data.twitter_account.substring(1) : data.twitter_account}
          options={{height: 600, width: 1000}}
          theme="dark"
          placeholder={<div style={{ display: "flex" }}><h3 style={{ marginRight: "20px" }}>Loading Twitter feed...</h3><Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner></div>}
        />}
      <ModelLinks stockData={stockData} committeeData={committeeData} />
    </div>
  );
}
