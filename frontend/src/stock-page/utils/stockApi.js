import axios from "axios";
const stockData = require("../data/stocks.json");

export async function getStockDetailsById(id) {
  // const res = await axios.get(
  //   `https://10ff10e3-11e6-4518-a177-953ad276a3fc.mock.pstmn.io/api/stocks/symbol=${symbol}`
  // );
  const res = await axios.get(`https://capitolcapital.us/api/stocks/${id}`);
  //const data = stockData.find((obj) => obj.stock_id === id);
  // console.log(JSON.stringify(res, 0, 2));
  return res.data;
}

export async function getStocksDetailsByIdList(list) {
  var data = [];
  // TODO: jest fails when list == null
  if (!list) {
    return data;
  }
  for (let i = 0; i < list.length && i < 10; i++) {
    const obj = await getStockDetailsById(list[i]);
    data.push(obj);
  }
  return data;
}

export async function getStocksInRange(start, end) {
  const res = await axios.get(
    `https://capitolcapital.us/api/stocks/getStocksInRange?start=${start}&end=${end}`
  );
  // var data = stockData.slice(start, end);
  //return stockData.slice(0, 4);
  console.log(res.data.stocks[0]);
  return res.data.stocks;
}

//Make call for filtering
export async function getStockByFilter(
  start,
  end,
  sector,
  sortVal,
  sortOrder,
  search
) {
  let query = `https://capitolcapital.us/api/stocks/getStocksInRange?start=${start}&end=${end}`;
  if (sector !== "Sector") {
    query += `&sector=${sector}`;
  }
  if (sortVal !== "Sort By") {
    query += `&val=${sortVal}`;
  }
  if (sortOrder !== "Order By") {
    query += `&asc=${sortOrder == "Ascending" ? 1 : 0}`;
  }
  if (search !== "") {
    query += `&ser=${search}`;
  }

  const res = await axios.get(query);
  return res.data.stocks;
}
