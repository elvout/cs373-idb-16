import {
  PieChart,
  Pie,
  Legend,
  Tooltip,
  ResponsiveContainer,
  Cell,
} from "recharts";

export default function PPieChart(props) {
  const { data } = props;
  const COLORS = [
    "#0A1128",
    "#001F54",
    "#1282A2",
    "#DABFFF",
    "#907AD6",
    "#FAFF7F",
    "#FF5154",
    "#E88D67",
    "#F4B860",
  ];
  return (
    <ResponsiveContainer width={"99%"} height={400}>
      <PieChart width={390} height={390}>
        <Pie
          data={data}
          dataKey={"count"}
          outerRadius={150}
          fill={"#4b8cc4"}
          label={(obj) => obj.city}
        >
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Tooltip />
      </PieChart>
    </ResponsiveContainer>
  );
}
