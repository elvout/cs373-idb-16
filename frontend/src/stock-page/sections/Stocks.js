import { useState, useEffect, useReducer } from "react";
import StockCard from "../components/StockCard";
import { getStockByFilter } from "../utils/stockApi";
import { useNavigate, useSearchParams, useLocation } from "react-router-dom";
import CardPage from "../../components/CardPage";
import FilterDropdown from "../../components/FilterDropdown";

const questionImage =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Question_mark_%28black%29.svg/800px-Question_mark_%28black%29.svg.png";

const sectorOptions = [
  "Sector",
  "TECHNOLOGY",
  "TRADE & SERVICES",
  "MANUFACTURING",
  "ENERGY & TRANSPORTATION",
  "LIFE SCIENCES",
  "FINANCE",
];
const valOptions = ["Sort By", "StockPrice", "MarketCap"];
const ascOptions = ["Order By", "Ascending", "Descending"];

function reducer(state, action) {
  switch (action.type) {
    case "setSector": {
      return { ...state, sector: action.newSector.replace(" & ", "+%26+") };
    }
    case "setVal": {
      return { ...state, val: action.newVal };
    }
    case "setAsc": {
      return { ...state, asc: action.newAsc };
    }
    case "setSer": {
      return { ...state, ser: action.newSer };
    }
    case "setPage": {
      return { ...state, page: action.newPage };
    }
  }
}

export default function Stocks() {
  const [searchParams, setSearchParams] = useSearchParams();
  const [cards, setCards] = useState();

  const [filters, dispatch] = useReducer(reducer, {
    page: searchParams.get("start") ? searchParams.get("start") / 20 + 1 : 1,
    sector: searchParams.get("sector")
      ? searchParams.get("sector")
      : sectorOptions[0],
    val: searchParams.get("val") ? searchParams.get("val") : valOptions[0],
    asc: searchParams.get("asc") ? searchParams.get("asc") : ascOptions[0],
    ser: searchParams.get("ser") ? searchParams.get("ser") : "",
  });

  const numPerPage = 20;
  const navigate = useNavigate();
  const location = useLocation();

  const updateCards = (data) => {
    if (!data) {
      return;
    }
    const tempCards = data.map((obj) => {
      let symbol = obj.stock_id;
      let name = obj.name;
      let sector = obj.sector;
      let industry = obj.industry;

      if (filters.ser !== "") {
        symbol = symbol.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
        name = name.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
        sector = sector.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
        industry = industry.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
      }
      return (
        <StockCard
          key={obj.stock_id}
          symbol={symbol}
          name={name}
          price={obj.stock_price}
          image={
            !obj.logo_url || obj.logo_url === "" ? questionImage : obj.logo_url
          }
          marketCap={obj.market_cap}
          sector={sector}
          industry={industry}
        />
      );
    });
    setCards(tempCards);
  };

  const updateData = async () => {
    const end = filters.page * numPerPage;
    const start = end - numPerPage;
    const data = await getStockByFilter(
      start,
      end,
      filters.sector,
      filters.val,
      filters.asc,
      filters.ser
    );
    return data;
  };

  const getQueryFromState = () => {
    const end = filters.page * numPerPage;
    const start = end - numPerPage;

    var query = `/stocks?start=${start}&end=${end}`;
    if (filters.asc !== ascOptions[0]) {
      query += `&asc=${filters.asc === ascOptions[1] ? 1 : 0}`;
    }
    if (filters.sector != sectorOptions[0]) {
      query += `&sector=${filters.sector}`;
    }
    if (filters.val != valOptions[0]) {
      query += `&val=${filters.val}`;
    }
    if (filters.ser != "") {
      query += `&ser=${filters.ser}`;
    }
    return query;
  };

  const init = async () => {
    const data = await updateData();
    updateCards(data);
  };
  const onSearch = () => {
    const newQuery = getQueryFromState();
    navigate(newQuery);
  };
  const onChangePage = (page) => {
    dispatch({ type: "setPage", newPage: page });
  };
  const onReset = () => {
    navigate("/stocks");
    window.location.reload(false);
  };

  useEffect(() => {
    onSearch();
  }, [filters.page, filters.sector, filters.ser, filters.val, filters.asc]);

  useEffect(() => {
    init();
  }, [location]);

  useEffect(() => {
    init();
  }, []);

  const filterComponents = [
    <FilterDropdown
      key={1}
      title={filters.sector}
      items={sectorOptions}
      onChange={(value) => {
        dispatch({ type: "setSector", newSector: value });
      }}
    />,
    <FilterDropdown
      key={2}
      title={filters.asc == 1 ? ascOptions[1] : ascOptions[2]}
      items={ascOptions}
      onChange={(value) => {
        dispatch({ type: "setAsc", newAsc: value });
      }}
    />,
    <FilterDropdown
      key={3}
      title={filters.val}
      items={valOptions}
      onChange={(value) => {
        dispatch({ type: "setVal", newVal: value });
      }}
    />,
  ];
  return (
    <CardPage
      search={filters.ser}
      setSearch={(value) => {
        dispatch({ type: "setSer", newSer: value });
      }}
      filterComponents={filterComponents}
      cards={cards}
      reset={onReset}
      page={filters.page}
      numPerPage={numPerPage}
      changePage={onChangePage}
    />
  );
}
