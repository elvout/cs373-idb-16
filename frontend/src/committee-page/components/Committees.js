import { useState, useEffect, useReducer } from "react";
import { getCommitteeByFilter } from "../utils/committeeApi";
import FilterDropdown from "../../components/FilterDropdown";
import { useNavigate, useSearchParams, useLocation } from "react-router-dom";
import CommitteeCard from "./CommitteeCard";
import CardPage from "../../components/CardPage";

const ascOptions = ["Order By", "Ascending", "Descending"];
const valOptions = ["Sort By", "Congress", "NumberofMembers", "Investments", "StartDate"];
const chamberOptions = ["Chamber", "Senate", "House", "Joint"];

function reducer(state, action) {
  switch (action.type) {
    case "setVal": {
      return { ...state, val: action.newVal };
    }
    case "setAsc": {
      return { ...state, asc: action.newAsc };
    }
    case "setSer": {
      return { ...state, ser: action.newSer };
    }
    case "setPage": {
      return { ...state, page: action.newPage };
    }
    case "setChamber": {
      return { ...state, chamber: action.newChamber };
    }
  }
}

export default function Committees() {
  const [cards, setCards] = useState();
  const [searchParams, setSearchParams] = useSearchParams();

  const [filters, dispatch] = useReducer(reducer, {
    page: searchParams.get("start") ? searchParams.get("start") / 20 + 1 : 1,
    val: searchParams.get("val") ? searchParams.get("val") : valOptions[0],
    asc: searchParams.get("asc") ? searchParams.get("asc") : ascOptions[0],
    chamber: searchParams.get("chamber")
      ? searchParams.get("chamber")
      : chamberOptions[0],
    ser: searchParams.get("ser") ? searchParams.get("ser") : "",
  });

  const numPerPage = 20;
  const navigate = useNavigate();
  const location = useLocation();

  const updateCards = (data) => {
    const tempCards = data.map((obj, i) => {
      let name = obj.name;
      if (filters.ser !== "") {
        name = name.replace(
          new RegExp(filters.ser, "gi"),
          (match) => `<mark>${match}</mark>`
        );
      }
      return (
        <CommitteeCard
          key={i}
          id={obj.committee_id}
          name={name}
          logoUrl={obj.logo_url}
          chamber={obj.chamber}
          congress={obj.congress}
          size={obj.number_of_members}
          investments={obj.investments}
          year={obj.start_date}
        />
      );
    });
    setCards(tempCards);
  };

  const updateData = async () => {
    const end = filters.page * numPerPage;
    const start = end - numPerPage;
    const data = await getCommitteeByFilter(
      start,
      end,
      filters.asc,
      filters.val,
      filters.ser,
      filters.chamber
    );
    return data;
  };

  const init = async () => {
    const data = await updateData();
    updateCards(data);
  };
  const onSearch = () => {
    const newQuery = getQueryFromState();
    navigate(newQuery);
  };
  const onChangePage = (page) => {
    dispatch({ type: "setPage", newPage: page });
  };
  const onReset = () => {
    navigate("/committees");
    window.location.reload(false);
  };

  useEffect(() => {
    onSearch();
  }, [filters.page, filters.ser, filters.val, filters.asc, filters.chamber]);

  useEffect(() => {
    init();
  }, [location]);

  useEffect(() => {
    init();
  }, []);

  const filterComponents = [
    <FilterDropdown
      title={filters.asc}
      items={ascOptions}
      onChange={(value) => {
        dispatch({ type: "setAsc", newAsc: value });
      }}
    />,
    <FilterDropdown
      title={filters.val}
      items={valOptions}
      onChange={(value) => {
        dispatch({ type: "setVal", newVal: value });
      }}
    />,
    <FilterDropdown
      title={filters.chamber}
      items={chamberOptions}
      onChange={(value) => {
        dispatch({ type: "setChamber", newChamber: value });
      }}
    />,
  ];
  const getQueryFromState = () => {
    const end = filters.page * numPerPage;
    const start = end - numPerPage;
    var query = `/committees?start=${start}&end=${end}`;
    if (filters.asc != ascOptions[0]) {
      query += `&asc=${filters.asc == "Ascending" ? 1 : 0}`;
    }
    if (filters.val !== valOptions[0]) {
      query += `&val=${filters.val}`;
    }
    if (filters.ser !== "") {
      query += `&ser=${filters.ser}`;
    }
    if (filters.chamber !== "Chamber") {
      query += `&chamber=${filters.chamber}`;
    }
    return query;
  };
  return (
    <CardPage
      search={filters.ser}
      setSearch={(value) => {
        dispatch({ type: "setSer", newSer: value });
      }}
      filterComponents={filterComponents}
      cards={cards}
      reset={onReset}
      page={filters.page}
      numPerPage={numPerPage}
      changePage={onChangePage}
    />
  );
}
