import axios from "axios";

const politicianData = require("../data/politicians.json");

export async function getPoliticiansInRange(start, end) {
  // const res = await axios.get(
  //   `https://10ff10e3-11e6-4518-a177-953ad276a3fc.mock.pstmn.io/api/stocks/getStocksInRangestart=${start}&end=${end}`
  // );
  const res = await axios.get(
    `https://capitolcapital.us/api/politicians/getPoliticiansInRange?start=${start}&end=${end}`
  );
  //var data = politicianData.slice(start, end);
  return res.data.politicians;
}

export async function getPoliticianDetailsById(id) {
  const res = await axios.get(
    `https://capitolcapital.us/api/politicians/${id}`
  );
  //const data = politicianData.find((obj) => obj.politician_id === id);
  return res.data;
}

export async function getPoliticianDetailsByIdList(list) {
  var data = [];
  // TODO: jest fails when list == null
  if (!list) {
    return data;
  }
  for (let i = 0; i < list.length && i < 10; i++) {
    const obj = await getPoliticianDetailsById(list[i]);
    data.push(obj);
  }
  return data;
}

//Make call for filtering
export async function getPoliticianByFilter(
  start,
  end,
  sortOrder,
  party,
  gender,
  sortVal,
  search
) {
  let query = `https://capitolcapital.us/api/politicians/getPoliticiansInRange?start=${start}&end=${end}`;

  //Build query string to fetch data based on passed parameters
  if (sortOrder) {
    query = sortOrder == "Descending" ? query + "&asc=0" : query + "&asc=1";
  }
  if (party != "Party") {
    query += `&Party=${party}`;
  }
  if (gender != "Gender") {
    query += `&Gender=${gender}`;
  }
  if (sortVal != "SortVal") {
    query += `&val=${sortVal}`;
  }
  if (search != "") {
    query += `&ser=${search}`;
  }

  const res = await axios.get(query);
  return res.data.politicians;
}
