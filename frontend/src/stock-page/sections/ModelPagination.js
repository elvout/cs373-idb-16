import Pagination from "react-bootstrap/Pagination";

export default function StockPagination({numElems, numElemsPerPage, paginationHandler, active}){
  let pages = [];

  //TODO: Create pagination 
  const numPages = Math.ceil(numElems/numElemsPerPage);
  for (let number = 1; number <= numPages; number++) {
      pages.push(
      <Pagination.Item
          key={number}
          active={number === active}
          onClick={() => paginationHandler(number)}
      >
          {number}
      </Pagination.Item>
      );
  }

  return(
    <div style={{display: "flex", justifyContent: "center"}}>
      <Pagination size="md">
        <Pagination.Prev
          onClick={() => {
            if (active > 1) {
              paginationHandler(active - 1);
            }
          }}
        />
          {pages}
        <Pagination.Next
          onClick={() => {
          if (active < 5) {
            paginationHandler(active + 1);
          }
          }}
          />
      </Pagination>      
    </div>
  )


}