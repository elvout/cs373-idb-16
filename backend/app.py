import json

from flask import Flask, Response, request
from flask_restless import APIManager

import database


"""
define Database -> JSON form
"""


def politicians_to_Json(d):
    v = []
    for r in d:
        json_object = {
            "politician_id": r[0],
            "name": r[1],
            "gender": r[2],
            "party": r[3],
            "date_of_birth": r[4],
            "first_elected": r[5],
            "cash_on_hand": r[6],
            "debt": r[7],
            "img_url": r[8],
            "banner_url": r[9],
            "url": r[10],
            "twitter_account": r[11],
            "stocks_traded": r[12],
            "committees_membership": r[13],
        }
        v.append(json_object)
    return v


def stocks_to_Json(d):
    v = []
    for r in d:
        json_object = {
            "stock_id": r[0],
            "name": r[1],
            "stock_price": r[2],
            "market_cap": r[3],
            "sector": r[4],
            "industry": r[5],
            "image_url": r[6],
            "logo_url": r[7],
            "twitter_account": r[8],
            "address": r[9],
            "description": r[10],
            "traded_politicians": r[11],
            "traded_committees": r[12],
        }
        v.append(json_object)
    return v


def committees_to_Json(d):
    v = []
    for r in d:
        json_object = {
            "committee_id": r[0],
            "congress": r[1],
            "name": r[2],
            "chamber": r[3],
            "start_date": r[4],
            "url": r[5],
            "chair": r[6],
            "number_of_members": r[7],
            "logo_url": r[8],
            "image_url": r[9],
            "twitter_account": r[10],
            "investments": r[11],
            "members": r[12],
            "stocks_traded": r[13],
        }
        v.append(json_object)
    return v


# API Stuff here
app = Flask(__name__)
manager = APIManager(app, session=database.mysession)


"""
For Politicians API
"""


# https://10ff10e3-11e6-4518-a177-953ad276a3fc.mock.pstmn.io/api/stocks/getStocksInRange/?start=${start}&end=${end}
@app.route("/api/politicians/getPoliticiansInRange")
def get() -> Response:
    start = request.args.get("start", default=0, type=int)
    end = request.args.get("end", default=0, type=int) + 1
    asc = request.args.get("asc", default=1, type=int)
    val = request.args.get("val", default="PoliticianID", type=str)
    ser = request.args.get("ser", default="", type=str)
    minNumber = request.args.get("minNumber", default=0, type=int)
    maxNumber = request.args.get("maxNumber", default=3000, type=int)
    Gender = request.args.get("Gender", default="", type=str)
    Party = request.args.get("Party", default="", type=str)
    c = database.getPoliticiansCount(ser, minNumber, maxNumber, Gender, Party)
    d = database.getPoliticians(
        start, end, asc, val, ser, minNumber, maxNumber, Gender, Party
    )
    v = politicians_to_Json(d)
    json_object = {"count": c, "politicians": v}
    return Response(json.dumps(json_object), mimetype="application/json")


@app.route("/api/politicians/<id>")
def g(id: str) -> Response:
    value = database.getPoliticiansbyID(id)

    if len(value) != 0:
        r = value[0]
        json_object = politicians_to_Json([r])[0]
    else:
        json_object = {}
    return Response(json.dumps(json_object), mimetype="application/json")


@app.route("/api/politicians/member/<id>")
def polmem(id):
    for r in database.getPoliticiansMembership(id):
        json_object = {
            "membership": r[0],
        }
        json_string = json.dumps(json_object)
        return json_string


"""
For Stocks API
"""


# https://10ff10e3-11e6-4518-a177-953ad276a3fc.mock.pstmn.io/api/stocks/getStocksInRange/?start=${start}&end=${end}
@app.route("/api/stocks/getStocksInRange")
def get1() -> Response:
    start = request.args.get("start", default=0, type=int)
    end = request.args.get("end", default=0, type=int) + 1
    asc = request.args.get("asc", default=1, type=int)
    val = request.args.get("val", default="StockID", type=str)
    ser = request.args.get("ser", default="", type=str)
    minNumber = request.args.get("minNumber", default=0, type=int)
    maxNumber = request.args.get("maxNumber", default=3000, type=int)
    sector = request.args.get("sector", default="", type=str)
    c = database.getStocksCount(ser, minNumber, maxNumber, sector)
    d = database.getStocks(start, end, asc, val, ser, minNumber, maxNumber, sector)
    v = stocks_to_Json(d)
    json_object = {"count": c, "stocks": v}
    return Response(json.dumps(json_object), mimetype="application/json")


@app.route("/api/stocks/<id>")
def g1(id: str) -> Response:
    value = database.getStocksbyID(id)

    if len(value) != 0:
        r = value[0]
        json_object = stocks_to_Json([r])[0]
    else:
        json_object = {}
    return Response(json.dumps(json_object), mimetype="application/json")


"""
For Committees API
"""


# https://10ff10e3-11e6-4518-a177-953ad276a3fc.mock.pstmn.io/api/politicians/getPoliticiansInRangestart=${start}&end=${end}
@app.route("/api/committees/getCommitteesInRange")
def get2() -> Response:
    start = request.args.get("start", default=0, type=int)
    end = request.args.get("end", default=0, type=int) + 1
    asc = request.args.get("asc", default=1, type=int)
    val = request.args.get("val", default="CommitteesID", type=str)
    ser = request.args.get("ser", default="", type=str)
    minNumber = request.args.get("minNumber", default=0, type=int)
    maxNumber = request.args.get("maxNumber", default=2500, type=int)
    cham = request.args.get("chamber", default="", type=str)
    c = database.getCommitteesCount(ser, minNumber, maxNumber, cham)
    d = database.getCommittees(start, end, asc, val, ser, minNumber, maxNumber, cham)
    v = committees_to_Json(d)
    json_object = {"count": c, "committees": v}
    return Response(json.dumps(json_object), mimetype="application/json")


@app.route("/api/committees/<id>")
def g2(id: str) -> Response:
    value = database.getCommitteesbyID(id)

    if len(value) != 0:
        r = value[0]
        json_object = committees_to_Json([r])[0]
    else:
        json_object = {}
    return Response(json.dumps(json_object), mimetype="application/json")


@app.route("/api/analasys1")
def get123() -> Response:
    s1 = {"stock": "Apple Inc", "amount": 65}
    s2 = {"stock": "Microsoft Corporation", "amount": 54}
    s3 = {"stock": "Amazon.com Inc", "amount": 46}
    s4 = {"stock": "Walt Disney Company", "amount": 40}
    s5 = {"stock": "Meta Platforms Inc.", "amount": 37}
    s6 = {"stock": "Procter & Gamble Company", "amount": 36}
    s7 = {"stock": "Johnson & Johnson", "amount": 36}
    s8 = {"stock": "Verizon Communications Inc", "amount": 35}
    s9 = {"stock": "JPMorgan Chase & Co", "amount": 35}
    s10 = {"stock": "AT&T Inc", "amount": 35}
    a = [s1, s2, s3, s4, s5, s6, s7, s8, s9, s10]
    return Response(json.dumps({"val": a}), mimetype="application/json")


@app.route("/api/analasys2")
def get1234() -> Response:
    s1 = [
        (51, 190),
        (22, 620),
        (11, 165),
        (44, 187),
        (43, 85),
        (9, 190),
        (26, 170),
        (57, 599),
        (30, 278),
        (3, 2),
        (17, 162),
        (28, 175),
        (4, 43),
        (10, 45),
        (35, 221),
        (23, 73),
        (35, 221),
        (6, 119),
        (39, 221),
        (36, 221),
        (50, 257),
        (14, 144),
        (66, 133),
        (13, 128),
        (46, 272),
        (53, 179),
        (7, 111),
        (12, 82),
        (8, 138),
        (25, 157),
        (20, 543),
        (1, 2),
        (18, 111),
        (27, 143),
    ]
    a = []
    for i in s1:
        s = {"numberCom": i[0], "amount": i[1]}
        a.append(s)
    return Response(json.dumps({"val": a}), mimetype="application/json")


@app.route("/api/analasys3")
def get12345() -> Response:
    s1 = [
        (2010, 3),
        (1991, 26),
        (2019, 446),
        (1987, 95),
        (2011, 201),
        (1985, 2),
        (1983, 67),
        (2017, 405),
        (2009, 156),
        (2005, 71),
        (2013, 274),
        (2003, 21),
        (2015, 122),
        (1993, 68),
        (1981, 42),
        (2007, 299),
        (2002, 5),
        (2021, 164),
        (1997, 130),
        (2020, 120),
        (2001, 120),
        (2016, 29),
        (1999, 47),
        (2012, 2),
        (2018, 87),
        (1995, 147),
        (2021, 521),
    ]
    a = []
    for i in s1:
        s = {"year": i[0], "amount": i[1]}
        a.append(s)
    return Response(json.dumps({"val": a}), mimetype="application/json")


# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
if __name__ == "__main__":
    manager.create_api(database.politicians, methods=["GET"])
    manager.create_api(database.stocks, methods=["GET"])
    manager.create_api(database.committees, methods=["GET"])
    # start the flask loop
    # from waitress import serve
    # serve(app, host="0.0.0.0", port=5000)
    app.run(host="0.0.0.0", port=5000)
