import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { LinkContainer } from "react-router-bootstrap";
import capitol from "./images/capitol-building.png";

export default function NavigationBar() {
  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container>
        <Navbar.Brand>
          <img
            src={capitol}
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <LinkContainer to="/">
          <Navbar.Brand>Capitol Capital</Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <LinkContainer to="/stocks">
              <Nav.Link>Stocks</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/politicians">
              <Nav.Link>Politicians</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/committees">
              <Nav.Link>Committees</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/about">
              <Nav.Link>About</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/visualization">
              <Nav.Link>Visualizations</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/providervisualization">
              <Nav.Link>Provider Visualizations</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
