import axios from "axios";
import teamData from "../data/teamData";

async function getGitlabCommits() {
  const res = await axios.get(
    "https://gitlab.com/api/v4/projects/43419203/repository/commits?ref_name=main&per_page=10000"
  );
  return res;
}
async function getGitlabIssues() {
  const res = await axios.get(
    "https://gitlab.com/api/v4/projects/43419203/issues"
  );
  return res;
}
function updateGitlabCommits(data) {
  var mapNamesCommits = {};
  data.forEach((commit) => {
    const name = commit.committer_name;
    if (name in mapNamesCommits) {
      mapNamesCommits[name]++;
    } else {
      mapNamesCommits[name] = 1;
    }
  });
  console.log(mapNamesCommits);
  for (let name in mapNamesCommits) {
    teamData.data.forEach((member) => {
      if (name === member.name || member.alt_names.has(name)) {
        member.gitlab.commits += mapNamesCommits[name];
      }
    });
  }
}
function updateGitlabIssues(data) {
  var mapUsernamesDataObj = {};
  teamData.data.forEach((member) => {
    mapUsernamesDataObj[member.gitlab.username] = member;
  });
  console.log(data);
  data.forEach((issue) => {
    let creditedUsername = "";

    if (issue.assignee !== null) {
      creditedUsername = issue.assignee.username;
    } else if (issue.closed_by !== null) {
      creditedUsername = issue.closed_by.username;
    } else if (issue.author !== null) {
      creditedUsername = issue.author.username;
    }

    if (creditedUsername in mapUsernamesDataObj) {
      mapUsernamesDataObj[creditedUsername].gitlab.issues++;
    }
  });
}

var gitlabAlreadyUpdated = false;
export async function updateGitlabStatistics() {
  if (gitlabAlreadyUpdated) {
    return;
  }
  gitlabAlreadyUpdated = true;

  const res1 = await getGitlabCommits();
  updateGitlabCommits(res1.data);
  const res2 = await getGitlabIssues();
  updateGitlabIssues(res2.data);
}
