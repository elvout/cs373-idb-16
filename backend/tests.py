import unittest

import app


class Tests(unittest.TestCase):
    def setUp(self) -> None:
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def test_get_politicians_by_range(self) -> None:
        with self.client:
            response = self.client.get(
                "/api/politicians/getPoliticiansInRange?start=1&end=10"
            )
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json  # for mypy type checking
            json_body = response.json
            self.assertEqual(len(json_body["politicians"]), 10)

    def test_get_politician_by_id(self) -> None:
        with self.client:
            response = self.client.get("/api/politicians/A000370")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json is not None  # for mypy type checking
            json_body = response.json
            self.assertEqual(json_body["politician_id"], "A000370")
            self.assertEqual(json_body["name"], "Alma Adams")

    def test_get_politician_by_id_that_does_not_exist(self) -> None:
        with self.client:
            response = self.client.get("/api/politicians/ABCDEFG")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json is not None  # for mypy type checking
            json_body = response.json
            self.assertEqual(len(json_body), 0)

    def test_get_stocks_by_range(self) -> None:
        with self.client:
            response = self.client.get("/api/stocks/getStocksInRange?start=1&end=10")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json  # for mypy type checking
            json_body = response.json
            self.assertEqual(len(json_body["stocks"]), 10)

    def test_get_stock_by_id(self) -> None:
        with self.client:
            response = self.client.get("/api/stocks/AAPL")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json is not None  # for mypy type checking
            json_body = response.json
            self.assertEqual(json_body["stock_id"], "AAPL")
            self.assertEqual(json_body["name"], "Apple Inc")

    def test_get_stock_by_id_that_does_not_exist(self) -> None:
        with self.client:
            response = self.client.get("/api/stocks/ABCDEFG")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json is not None  # for mypy type checking
            json_body = response.json
            self.assertEqual(len(json_body), 0)

    def test_get_committees_by_range(self) -> None:
        with self.client:
            response = self.client.get(
                "/api/committees/getCommitteesInRange?start=1&end=10"
            )
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json  # for mypy type checking
            json_body = response.json
            self.assertEqual(len(json_body["committees"]), 10)

    def test_get_committee_by_id(self) -> None:
        with self.client:
            response = self.client.get("/api/committees/SSBK")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json is not None  # for mypy type checking
            json_body = response.json
            self.assertEqual(json_body["committee_id"], "SSBK")
            self.assertEqual(
                json_body["name"], "Committee on Banking, Housing, and Urban Affairs"
            )

    def test_get_committee_by_id_that_does_not_exist(self) -> None:
        with self.client:
            response = self.client.get("/api/committees/ABCDEFG")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.is_json)
            assert response.json is not None  # for mypy type checking
            json_body = response.json
            self.assertEqual(len(json_body), 0)


if __name__ == "__main__":
    unittest.main()
