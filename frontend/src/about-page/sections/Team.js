import TeamCard from "../components/TeamCard";
import teamData from "../data/teamData";
import { useState, useEffect } from "react";
import { updateGitlabStatistics } from "../utils/gitlabApi";
import { Button } from "../../components/Buttons";

const phaseLeaders = [
  "Noah Kester",
  "Elvin Yang",
  "Jeffery Liu",
  "Milaan Dahiya",
];

function TotalGitLabStats() {
  let totalCommits = 0;
  let totalIssues = 0;
  let totalUnitTests = 0;
  teamData.data.forEach((member) => {
    totalCommits += member.gitlab.commits;
    totalIssues += member.gitlab.issues;
    totalUnitTests += member.gitlab.unitTests;
  });

  return (
    <div>
      <div class="h2">{"GitLab Stats"}</div>
      <div class="h4">{`Total Commits: ${totalCommits}`}</div>
      <div class="h4">{`Total Issues ${totalIssues}`}</div>
      <div class="h4">{`Total Unit Tests: ${totalUnitTests}`}</div>
    </div>
  );
}
function PhaseLeaders(props) {
  const elements = phaseLeaders.map((name, index) => {
    return <div class="h4">{`Phase ${index + 1}: ${name}`}</div>;
  });
  return (
    <div style={{ marginLeft: "10%" }}>
      <div class="h2">{"Phase Leaders"}</div>
      {elements}
    </div>
  );
}

function TeamCards(props) {
  const { showGitlab } = props;
  const [cards, setCards] = useState();
  console.log(showGitlab);
  useEffect(() => {
    const tempCards = teamData.data.map((object) => {
      return (
        <TeamCard
          profileUrl={object.profileUrl}
          name={object.name}
          role={object.role}
          about={object.about}
          gitlab={object.gitlab}
          showGitlab={showGitlab}
        />
      );
    });
    setCards(tempCards);
  }, [showGitlab]);

  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        width: "100%",
        justifyContent: "center",
        backgroundColor: "#F6F6F6",
      }}
    >
      {cards}
    </div>
  );
}

export default function Team() {
  const [totals, setTotals] = useState();
  const [showGitlab, setShowGitlab] = useState(false);

  useEffect(() => {
    async function update() {
      await updateGitlabStatistics();
      setTotals(TotalGitLabStats());
    }
    update();
  }, []);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
      }}
    >
      <div style={{ marginTop: 100, fontWeight: 500, fontSize: 60 }}>
        Our Team
      </div>
      <TeamCards showGitlab={showGitlab} />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          width: "100%",
          marginTop: 100,
        }}
      >
        <TotalGitLabStats />
        <PhaseLeaders />
      </div>
      <div style={{ marginTop: 30, marginBottom: 30 }}>
        <Button
          label={"Gitlab Stats"}
          onClick={() => {
            setShowGitlab(!showGitlab);
          }}
        />
      </div>
    </div>
  );
}
