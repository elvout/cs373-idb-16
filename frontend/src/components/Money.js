export function formatNumToMoney(num) {
  if (!num) {
    return "$0";
  }
  return "$" + num.toLocaleString("en-US");
}

export function getRandomMoney(max) {
  const min = 0;
  const num = Math.floor(Math.random() * (max - min + 1) + min);
  return formatNumToMoney(num);
}
