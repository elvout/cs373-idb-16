import { useLocation } from "react-router-dom";
import { getStockDetailsById } from "../utils/stockApi";
import { useState, useEffect } from "react";

import Spinner from "react-bootstrap/Spinner";
import Header from "./components/Header";
import StockInfo from "./components/StockInfo";
import CompanyStats from "./components/CompanyStats";
import ModelLinks from "./components/ModelLinks";

import { getPoliticianDetailsByIdList } from "../../politicians-page/utils/politicianApi";
import { getCommitteeDetailsByIdList } from "../../committee-page/utils/committeeApi";

export default function SingleStockPage(props) {
  const [data, setData] = useState();
  const [politicianData, setPoliticianData] = useState();
  const [committeeData, setCommitteeData] = useState();

  const location = useLocation();
  const symbol = location.pathname.split("/")[2];

  const updateData = async () => {
    const tempStockData = await getStockDetailsById(symbol);
    const tempPoliticianData = await getPoliticianDetailsByIdList(
      tempStockData.traded_politicians
    );
    const tempCommitteeData = await getCommitteeDetailsByIdList(
      tempStockData.traded_committees
    );

    setData(tempStockData);
    setPoliticianData(tempPoliticianData);
    setCommitteeData(tempCommitteeData);
  };

  useEffect(() => {
    updateData();
  }, []);

  if (!data || !politicianData || !committeeData) {
    return (
      <div
        style={{
          width: "100%",
          height: "100vh",
          display: "flex",
          flexDirection: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    );
  }
  return (
    <div style={{ padding: "4%" }}>
      <Header
        imgUrl={data.logo_url}
        symbol={data.stock_id}
        name={data.name}
        description={data.description}
      />
      <StockInfo
        bookValue={data.stock_price}
        marketCapitalization={data.market_cap}
      />
      <img
        src={
          data.image_url === null
            ? "https://www.resolutionlawng.com/wp-content/uploads/2020/09/group-of-company-img.jpeg"
            : data.image_url
        }
        style={{
          width: "100%",
          height: 300,
          objectFit: "cover",
        }}
      />
      <CompanyStats
        sector={data.sector}
        industry={data.industry}
        address={data.address}
      />
      <ModelLinks
        politicianData={politicianData}
        committeeData={committeeData}
      />
    </div>
  );
}
