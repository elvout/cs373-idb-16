import { useLocation } from "react-router-dom";
import { useState, useEffect } from "react";
import Header from "./components/Header";
import OfficialData from "./components/OfficialData";
import Investments from "./components/Investments";
import ModelLinks from "./components/ModelLinks";
import Spinner from "react-bootstrap/Spinner";
import { getCommitteeDetailsById } from "../utils/committeeApi";
import { getStocksDetailsByIdList } from "../../stock-page/utils/stockApi";
import { getPoliticianDetailsByIdList } from "../../politicians-page/utils/politicianApi";
import getRandomCommitteeBackground from "../utils/committeeImages";
import { TwitterTimelineEmbed } from "react-twitter-embed";

export default function SingleCommitteePage() {
  const [data, setData] = useState();
  const [politicianData, setPoliticianData] = useState();
  const [stockData, setStockData] = useState();

  const location = useLocation();
  const id = location.pathname.split("/")[2];

  const updateData = async () => {
    const tempCommitteeData = await getCommitteeDetailsById(id);

    const tempStockData = await getStocksDetailsByIdList(
      tempCommitteeData.stocks_traded
    );
    const tempPoliticianData = await getPoliticianDetailsByIdList(
      tempCommitteeData.members
    );

    setData(tempCommitteeData);
    setStockData(tempStockData);
    setPoliticianData(tempPoliticianData);
  };
  useEffect(() => {
    updateData();
  }, []);

  if (!data || !politicianData || !stockData) {
    return (
      <div
        style={{
          width: "100%",
          height: "100vh",
          display: "flex",
          flexDirection: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    );
  }
  return (
    <div style={{ padding: "4%" }}>
      <Header imgUrl={data.logo_url} id={data.committee_id} name={data.name} />
      <img
        src={
          data.image_url == null ? getRandomCommitteeBackground() : data.image_url
        }
        style={{
          width: "100%",
          height: 300,
          objectFit: "cover",
        }}
      />
      <OfficialData
        chamber={data.chamber}
        date={new Date()}
        size={data.number_of_members}
        congress={data.congress}
      />
      {data.twitter_account == null ? <div/> :
        <TwitterTimelineEmbed
          sourceType="profile"
          screenName={data.twitter_account.substring(0, 1) == "@" ? data.twitter_account.substring(1) : data.twitter_account}
          options={{height: 600, width: 1000}}
          theme="dark"
          placeholder={<div style={{ display: "flex" }}><h3 style={{ marginRight: "20px" }}>Loading Twitter feed...</h3><Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner></div>}
        />}
      <Investments cash={"123,456,7890"} />
      <ModelLinks stockData={stockData} politicianData={politicianData} />
    </div>
  );
}
