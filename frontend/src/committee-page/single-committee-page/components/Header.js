export default function Header(props) {
  const { imgUrl, id, name } = props;
  return (
    <div>
      <img
        src={imgUrl}
        style={{height: 100, objectFit: "cover" }}
      />
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <div class="h1">{name}</div>
        <div style={{ marginLeft: 20, fontSize: 14 }}>{"Add bio"}</div>
      </div>
      <div class="h3">{id}</div>
    </div>
  );
}
