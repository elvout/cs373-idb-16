import Page from "./components/Page";
import Graphic from "./components/Graphic";
import VBarChart from "./components/VBarChart";
import VLineChart from "./components/VLineChart";
import VScatterChart from "./components/VScatterChart";
import {
  getTopBoughtStocks,
  getCommitteeSizeVsStocks,
  getYearsVsStocks,
} from "./utils/visualsApi";
import { useEffect, useState } from "react";

export default function VisualizationPage() {
  const [data1, setData1] = useState([]);
  const [data2, setData2] = useState([]);
  const [data3, setData3] = useState([]);
  const getData = async () => {
    const temp1 = await getTopBoughtStocks();
    setData1(temp1);
    const temp2 = await getCommitteeSizeVsStocks();
    setData2(temp2);
    const temp3 = await getYearsVsStocks();
    setData3(temp3);
  };

  useEffect(() => {
    getData();
  }, []);

  const graphics = (
    <div style={{ width: "100%" }}>
      <Graphic
        title={"Top stocks owned"}
        description={
          "We are not surprised that tech companies are among the top stocks owned with Apple and Microsoft, but we did not expect to see Disney so high. We also thought it was strange that so much of Congress was invested in Meta whenever there was a hearing a few years ago with Mark Zuckerberg to potentially add more regulation for Facebook"
        }
        graph={<VBarChart data={data1} />}
      />
      <Graphic
        title={"Committee size vs stock quantity"}
        description={
          "We expected there to be a strong positive correlation between the size of committee and the diversity of stocks held. However, the data seems a lot more random and there was a spike between 20 and 22 for the size of the committee. This could be caused by an outlier in the politicians with a single individual holding a very diverse portfolio. We also think the data between 35 and 39 was significant because the odds of the stocks being the exact same are really low so we think that the same members are involved in 3 different committees under different official names and purposes"
        }
        graph={<VLineChart data={data2} />}
      />
      <Graphic
        title={"Years elected vs stock holdings"}
        description={
          "We did not expect the trend of stock holdings to decrease with the age of politicians. It seems based on the data that younger politicians actually had more stocks in their portfolio. It is worth mentioning that this is the types of stocks and not the actual holdings so it still is possible that the older politicians have more stock value but less diversity."
        }
        graph={<VScatterChart data={data3} />}
      />
    </div>
  );
  return (
    <Page
      title={"Visualizations"}
      description={
        "Visualize the collected data from Capitol Capital for stocks, politicians, and committes"
      }
      graphics={graphics}
    />
  );
}
