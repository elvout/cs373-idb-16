import Mission from "./sections/Mission";
import Team from "./sections/Team";
import Apis from "./sections/Apis";
import Toolchain from "./sections/Toolchain";
import Documentation from "./sections/Documentation";

export default function AboutPage() {
  return (
    <div>
      <Mission />
      <Team />
      <Apis />
      <Toolchain />
      <Documentation />
    </div>
  );
}
