import { useState } from "react";
import { IoInformationCircleOutline } from "react-icons/io5";

export default function TeamCard(props) {
  const { profileUrl, name, role, about, gitlab, showGitlab } = props;
  console.log(showGitlab);
  const [showAbout, setShowAbout] = useState(false);

  const profileView = (
    <div
      style={{
        width: "90%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        padding: 10,
      }}
    >
      <img style={{ width: "100%", borderRadius: 10 }} src={profileUrl}></img>
      <div style={{ marginTop: 10 }} class="h3">
        {name}
      </div>
      <div class="h6">@{gitlab.username}</div>
      <div class="h6">{role}</div>
      <div
        style={{ cursor: "pointer" }}
        onClick={() => {
          setShowAbout(!showAbout);
        }}
      >
        <IoInformationCircleOutline color={"#262626"} size={20} />
      </div>
      {showAbout ? (
        <div
          style={{
            backgroundColor: "#F2F7F6",
            padding: 12,
            borderRadius: 10,
            marginBottom: 12,
            marginTop: 12,
          }}
        >
          <div style={{ fontWeight: 500 }}>{about}</div>
        </div>
      ) : null}
      {showGitlab ? (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            backgroundColor: "#F2F7F6",
            padding: 12,
            borderRadius: 10,
            alignItems: "center",
          }}
        >
          <div style={{ fontWeight: 500 }}>{`Commits: ${gitlab.commits}`}</div>
          <div style={{ fontWeight: 500 }}>{`Issues: ${gitlab.issues}`}</div>
          <div
            style={{ fontWeight: 500 }}
          >{`Unit Tests: ${gitlab.unitTests}`}</div>
        </div>
      ) : null}
    </div>
  );

  return (
    <div
      style={{
        width: 240,
        borderRadius: 10,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#FFFFFF",
        // border: "1px #262626 solid",
        marginLeft: 12,
        marginRight: 12,
        marginTop: 30,
        marginBottom: 30,
      }}
    >
      {profileView}
    </div>
  );
}
