import { formatNumToMoney } from "../../../components/Money";

export default function StockInfo(props) {
  const { bookValue, marketCapitalization } = props;
  return (
    <div>
      <div className="h3">Stock Price</div>
      <div style={{ marginTop: -20, fontSize: 50, color: "#278225" }}>
        {bookValue}
      </div>
      <div
        className="h3"
        style={{ marginBottom: 40 }}
      >{`Market Cap: ${formatNumToMoney(marketCapitalization)}`}</div>
    </div>
  );
}
