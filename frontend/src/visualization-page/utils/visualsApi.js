import axios from "axios";

export async function getTopBoughtStocks() {
  const res = await axios.get(`https://capitolcapital.us/api/analasys1`);
  return res.data.val;
}
export async function getCommitteeSizeVsStocks() {
  const res = await axios.get(`https://capitolcapital.us/api/analasys2`);
  var ret = res.data.val;
  ret.sort((a, b) => a.numberCom - b.numberCom);
  return res.data.val;
}
export async function getYearsVsStocks() {
  const res = await axios.get(`https://capitolcapital.us/api/analasys3`);
  var ret = res.data.val;
  const currYear = new Date().getFullYear();
  ret.forEach((obj) => {
    obj.year = currYear - obj.year;
  });
  return res.data.val;
}

export async function getRentPrices() {
  const res = await axios.get(
    `https://api.futurfindr.me/housing?page=1&per_page=1000`
  );
  const ret = [
    { category: "$0-$1500", count: 0 },
    { category: "$1500-$3000", count: 0 },
    { category: "$3000-$5000", count: 0 },
    { category: "$5000-$10000", count: 0 },
    { category: "$10000+", count: 0 },
  ];
  res.data.data.forEach((obj) => {
    if (obj.price < 1500) ret[0].count++;
    else if (obj.price < 3000) ret[1].count++;
    else if (obj.price < 5000) ret[2].count++;
    else if (obj.price < 10000) ret[3].count++;
    else ret[4].count++;
  });
  return ret;
} //
export async function getCityJobs() {
  const res = await axios.get(
    "https://api.futurfindr.me/jobs?page=1&per_page=100000"
  );
  var ret = [];
  var map = {};
  res.data.data.forEach((obj) => {
    if (obj.city in map) {
      map[obj.city]++;
    } else {
      map[obj.city] = 1;
    }
  });
  for (var key in map) {
    ret.push({ city: key, count: map[key] });
  }
  ret.sort((a, b) => b.amount - a.amount);

  return ret.slice(0, 12);
} //
export async function getTuition() {
  const res = await axios.get(
    "https://api.futurfindr.me/colleges?page=1&per_page=10000"
  );
  var ret = [];
  res.data.data.forEach((obj) => {
    ret.push({ instate: obj.instate_tuition, outstate: obj.outstate_tuition });
  });
  return ret;
} //
