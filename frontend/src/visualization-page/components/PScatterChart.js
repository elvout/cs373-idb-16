import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

export default function PScatterChart(props) {
  const { data } = props;
  console.log(data);
  return (
    <ResponsiveContainer width={"90%"} height={400}>
      <ScatterChart margin={20}>
        <CartesianGrid />
        <XAxis type={"number"} dataKey={"instate"} name={"In-state"} />
        <YAxis type={"number"} dataKey={"outstate"} name={"Out-of-state"} />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Legend />
        <Scatter name="Tuition" data={data} fill={"#4b8cc4"} />
      </ScatterChart>
    </ResponsiveContainer>
  );
}
