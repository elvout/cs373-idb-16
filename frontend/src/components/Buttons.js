import { useState } from "react";

export function Button(props) {
  const { label, onClick } = props;
  const [hover, setHover] = useState(false);
  return (
    <div
      style={{
        height: 40,
        width: "fit-content",
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: hover ? "#333333" : "#000000",
        borderRadius: 100,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
      }}
      onMouseEnter={() => {
        setHover(true);
      }}
      onMouseLeave={() => {
        setHover(false);
      }}
      onClick={onClick}
    >
      <div style={{ color: "#FFFFFF", fontWeight: 600, fontSize: 16 }}>{label}</div>
    </div>
  );
}
