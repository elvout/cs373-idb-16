import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

export default function VScatterChart(props) {
  const { data } = props;
  console.log(data);
  return (
    <ResponsiveContainer width={"90%"} height={400}>
      <ScatterChart margin={20}>
        <CartesianGrid />
        <XAxis
          type={"number"}
          dataKey={"year"}
          name={"Years Elected"}
          unit={""}
        />
        <YAxis
          type={"number"}
          dataKey={"amount"}
          name={"Number of Stocks"}
        />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Legend />
        <Scatter name="Politicians" data={data} fill={"#4b8cc4"} />
      </ScatterChart>
    </ResponsiveContainer>
  );
}
