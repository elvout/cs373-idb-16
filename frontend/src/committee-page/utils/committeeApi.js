import axios from "axios";

const committeeData = require("../data/committees.json");

export async function getCommitteesInRange(start, end) {
  const res = await axios.get(
    `https://capitolcapital.us/api/committees/getCommitteesInRange?start=${start}&end=${end}`
  );
  //var data = committeeData.slice(start, end);
  return res.data.committees;
}

export async function getCommitteeDetailsById(id) {
  const res = await axios.get(`https://capitolcapital.us/api/committees/${id}`);
  //const data = committeeData.find((obj) => obj.committee_id === id);

  return res.data;
}

export async function getCommitteeDetailsByIdList(list) {
  var data = [];
  // TODO: jest fails when list == null
  if (!list) {
    return data;
  }
  for (let i = 0; i < list.length && i < 10; i++) {
    const obj = await getCommitteeDetailsById(list[i]);
    data.push(obj);
  }
  return data;
}

//Make call for filtering
export async function getCommitteeByFilter(
  start,
  end,
  sortOrder,
  sortVal,
  search,
  chamber
) {
  let query = `https://capitolcapital.us/api/committees/getCommitteesInRange?start=${start}&end=${end}`;

  //Build query string to fetch data based on passed parameters

  //TODO: Ask Hyun if you can add sort by committee or sort by congress?
  //TODO: Implement min and max number
  if (sortOrder !== "Order By") {
    query += `&asc=${sortOrder == "Ascending" ? 1 : 0}`;
  }
  if (sortVal !== "Sort By") {
    query += `&val=${sortVal}`;
  }
  if (search !== "") {
    query += `&ser=${search}`;
  }
  if (chamber !== "Chamber") {
    query += `&chamber=${chamber}`;
  }

  const res = await axios.get(query);
  return res.data.committees;
}
