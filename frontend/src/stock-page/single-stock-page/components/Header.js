export default function Header(props) {
  const { imgUrl, symbol, name, description } = props;
  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            width: 100,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <img src={imgUrl} style={{ width: 100, height: 100 }} />
          <div class="h3">{symbol}</div>
        </div>
        <div style={{ marginLeft: 50 }}>
          <div class="h1">{name}</div>
          <div style={{ fontSize: 14 }}>{description}</div>
        </div>
      </div>
    </div>
  );
}
