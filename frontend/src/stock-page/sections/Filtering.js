import React from "react";
// import { MDBCol, MDBIcon } from "mdbreact";
// https://dev.to/_martinwheeler_/create-a-react-search-bar-that-highlights-your-results-4hdh

import { FaSearch } from "react-icons/fa";

function SearchBar() {
  return (
    <div
      style={{
        height: 40,
        width: "100%",
        display: "flex",
        alignItems: "center",
      }}
    >
      <input style={{ height: "100%", width: "60%" }} />
      <div
        className="flex-center"
        style={{ height: "100%", width: 40, backgroundColor: "#3C8BE2" }}
      >
        <FaSearch color="#FFFFFF" size={"50%"} />
      </div>
    </div>
  );
}

export default function Filtering() {
  return (
    <div>
    </div>
  );
}
