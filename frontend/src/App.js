import NavigationBar from "./NavigationBar";
import HomePage from "./home-page/HomePage";
import StockPage from "./stock-page/StockPage";
import SingleStockPage from "./stock-page/single-stock-page/SingleStockPage";
import PoliticiansPage from "./politicians-page/PoliticiansPage";
import AboutPage from "./about-page/AboutPage";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import SinglePoliticianPage from "./politicians-page/single-politician-page/SinglePoliticianPage";
import CommitteesPage from "./committee-page/CommitteesPage";
import SingleCommitteePage from "./committee-page/single-committee-page/SingleCommitteePage";
import VisualizationPage from "./visualization-page/Visualization";
import ProviderPage from "./visualization-page/Provider";

function App() {
  return (
    <Router>
      <NavigationBar />
      <Routes>
        <Route exact path="/" element={<HomePage />} />
        <Route exact path="/stocks" element={<StockPage />} />
        <Route exact path="/stocks/:id" element={<SingleStockPage />} />
        <Route exact path="/politicians" element={<PoliticiansPage />} />
        <Route
          exact
          path="/politicians/:id"
          element={<SinglePoliticianPage />}
        />
        <Route exact path="/committees" element={<CommitteesPage />} />
        <Route exact path="/committees/:id" element={<SingleCommitteePage />} />
        <Route exact path="/about" element={<AboutPage />} />
        <Route exact path="/visualization" element={<VisualizationPage />} />
        <Route exact path="/providervisualization" element={<ProviderPage />} />
      </Routes>
    </Router>
  );
}

export default App;
