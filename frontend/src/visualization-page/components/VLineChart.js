import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

export default function VLineChart(props) {
  const { data } = props;
  return (
    <ResponsiveContainer width={"90%"} height={400}>
      <LineChart width={400} height={400} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="numberCom" />
        <YAxis />
        <Tooltip labelStyle={{ color: "black" }} />
        <Legend />
        <Line
          type="monotone"
          dataKey="amount"
          stroke="#4b8cc4"
          activeDot={{ r: 8 }}
        />
      </LineChart>
    </ResponsiveContainer>
  );
}
