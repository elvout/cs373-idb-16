import renderer from "react-test-renderer";
import TeamCard from "../about-page/components/TeamCard";
import Apis from "../about-page/sections/Apis";
import StockCard from "../stock-page/components/StockCard";
import PoliticiansCard from "../politicians-page/components/PoliticiansCard";
import CommitteesPage from "../committee-page/CommitteesPage";
import StockPage from "../stock-page/StockPage";
import PoliticiansPage from "../politicians-page/PoliticiansPage";
import AboutPage from "../about-page/AboutPage";
import HomePage from "../home-page/HomePage";
import SingleCommitteePage from "../committee-page/single-committee-page/SingleCommitteePage";
import SinglePoliticianPage from "../politicians-page/single-politician-page/SinglePoliticianPage";
import SingleStockPage from "../stock-page/single-stock-page/SingleStockPage";
import { MemoryRouter, Router } from "react-router-dom";
import Stocks from "../stock-page/sections/Stocks";
import Politicians from "../politicians-page/sections/Politicians";
import Committees from "../committee-page/components/Committees";

it("TeamCard Test", () => {
  const teamData = {
    profileUrl: "https://media.licdn.com/dms/image/D5603AQHJ17hTJzUvcA/profile-displayphoto-shrink_200_200/0/1676282547547?e=1681948800&v=beta&t=yE3U2Fv7767PoME9S8MdwJGCD6wbUZ_o8bUgjSdVQ_I",
    name: "Jeffrey",
    role: "Human",
    about: "Test this",
    gitlab: {
        username: "jeffreyliu1220",
        commits: 0,
        issues: 0,
        unitTests: 0,
      },
    showGitlab: true,
  };

  const component = renderer.create(
  <TeamCard
    profileUrl={teamData.profileUrl}
    name={teamData.name}
    role={teamData.role}
    about={teamData.about}
    gitlab={teamData.gitlab}
    showGitlab={teamData.showGitLab}
  />);
  expect(component.name = "Jeffrey");
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("API Section Test", () => {
  const component = renderer.create(
  <Apis/>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("StockCard Test", () => {
  const stockData = {
    symbol: "IBM",
    name: "International Business Machines",
    price: 24,
    image: "https://logo.clearbit.com/ibm.com",
  };

  const component = renderer.create(
  <MemoryRouter>
    <StockCard stock={stockData} />
  </MemoryRouter>
  );

  expect(component.symbol = "IBM");
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("PoliticiansCard Test", () => {
  const politicianData = {
    name: "TestPolitician",
    party: "D",
    holdings: 100,
    id: "abc",
    profileUrl: "https://logo.clearbit.com/ibm.com"
  };

  const component = renderer.create(
  <MemoryRouter>
    <PoliticiansCard
      name={politicianData.name}
      party={politicianData.party}
      holdings={politicianData.holdings}
      id={politicianData.id}
      profileUrl={politicianData.profileUrl}
     />
  </MemoryRouter>
  );

  expect(component.name = "TestPolitican");
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("CommitteesPage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <CommitteesPage></CommitteesPage>
    </MemoryRouter>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("StockPage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <StockPage></StockPage>
    </MemoryRouter>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});


it("PoliticiansPage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <PoliticiansPage></PoliticiansPage>
    </MemoryRouter>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});


it("AboutPage Test", () => {
  const component = renderer.create(
    <AboutPage></AboutPage>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("HomePage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <HomePage />
    </MemoryRouter>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("SingleCommitteePage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <SingleCommitteePage/>
    </MemoryRouter>

  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("SingleStockPage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <SingleStockPage/>
    </MemoryRouter>

  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("SinglePoliticianPage Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <SinglePoliticianPage/>
    </MemoryRouter>

  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("StockSortBy Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Stocks/>
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("StockSector Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Stocks/>
      expect(screen.getByText('Sector')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("StockReset Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Stocks/>
      expect(screen.getByText('Reset')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("PoliticiansSortBy Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Politicians/>
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("PoliticiansParty Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Politicians/>
      expect(screen.getByText('Party')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("PoliticiansGender Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Politicians/>
      expect(screen.getByText('Gender')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("PoliticiansReset Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Politicians/>
      expect(screen.getByText('Reset')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("CommitteesSortBy Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Committees/>
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("CommitteesChamber Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Committees/>
      expect(screen.getByText('Chamber')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("CommitteesOrderBy Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Committees/>
      expect(screen.getByText('Order By')).toBeInTheDocument();
    </MemoryRouter>
  );
});

it("CommitteesReset Test", () => {
  const component = renderer.create(
    <MemoryRouter>
      <Committees/>
      expect(screen.getByText('Reset')).toBeInTheDocument();
    </MemoryRouter>
  );
});
