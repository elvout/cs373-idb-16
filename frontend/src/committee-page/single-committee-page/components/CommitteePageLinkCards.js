import { Card, Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

export function StockCard(props) {
  const { symbol, name, price, image } = props;
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={image}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>{symbol}</Card.Title>
        <Card.Subtitle>{name}</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <LinkContainer to={`/stocks/${symbol}`}>
          <Button variant="primary">View Stock</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}
export function PoliticianCard(props) {
  const { id, name, imgUrl, party, lastBuy, totalVolume } = props;
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={imgUrl}
        style={{ height: 200, objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>{party}</Card.Subtitle>
        <Card.Text>{`Current holdings: ${totalVolume}`}</Card.Text>
        <LinkContainer to={`/politicians/${id}`}>
          <Button variant="primary">{"View"}</Button>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
}
