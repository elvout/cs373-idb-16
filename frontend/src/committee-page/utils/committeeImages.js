const images = [
  "https://www.chainbridgebank.com/assets/content/Jml2O9xo/Political%20Committee%20-1154438278.jpg",
  "https://www.bankrate.com/2022/09/21153957/political-donations-credit-card.jpg",
  "https://files.fop.net/wp-content/uploads/2021/03/fop-pac_1920px.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2WSzkuwCpu_dkRShOiigjWH_j_cQ0nTEsxt8dMF-6S9_41H073KtBsZJhyj9VSWrK4au6C9tVasM&usqp=CAU&ec=48600112",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSv5XHa2QYjmM_zdeQpQDwO2-9mOWXlkhMU3YVbQXcGUw&usqp=CAU&ec=48600112",
  "https://i.ytimg.com/vi/VHHw9m1mtq8/maxresdefault.jpg",
  "https://www.gannett-cdn.com/presto/2021/02/03/NNWF/900fe498-1664-49d2-8dd9-17b89ca36c33-fwb_forum4.jpg?crop=1784,1004,x15,y0&width=660&height=372&format=pjpg&auto=webp",
  "https://www.gannett-cdn.com/presto/2021/02/03/NNWF/4dba96c3-bd08-48c4-9193-db979dace60d-fwb_forum5.jpg?crop=1628,916,x0,y0&width=1600&height=800&format=pjpg&auto=webp",
  "https://www.grinnell.edu/sites/default/files/styles/landing_page_hero/public/images/2020-04/Zoom%20Backgrounds11.jpg?h=d1cb525d&itok=kfgTHS9F",
  "https://substackcdn.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fsubstack-post-media.s3.amazonaws.com%2Fpublic%2Fimages%2Fb0393dca-eeab-4abd-8547-29c4d311e73f_1200x798.png",
  "https://media.newyorker.com/photos/590977d8ebe912338a377edc/16:9/w_1903,h_1070,c_limit/160704_r28362.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyz2gVhtLMPYxG9Y1ZB1Z9gw1zrDWzmczjFAel1wrSoAUE7bWVHXeCUpEzjbQNxc9XovRK0LWCoM4&usqp=CAU&ec=48600112",
];
export default function getRandomCommitteeBackground() {
  const url = images[Math.floor(Math.random() * images.length)];
  return url;
}
