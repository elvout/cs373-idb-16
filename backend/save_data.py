import json
from pathlib import Path

from database import addCommittees, addPolitician, addStocks, dropAllRows

dropAllRows()
with open(Path("data/politicians.json")) as f:
    json_data = json.load(f)
    for r in json_data:
        addPolitician(
            r.get("politician_id"),
            r.get("name"),
            r.get("gender"),
            r.get("party"),
            r.get("date_of_birth"),
            r.get("first_elected"),
            r.get("cash_on_hand"),
            r.get("debt"),
            r.get("img_url"),
            r.get("banner_url"),
            r.get("url"),
            r.get("twitter_account"),
            r.get("stocks_traded"),
            r.get("committees_membership"),
        )

with open(Path("data/stocks.json")) as f:
    json_data = json.load(f)
    for r in json_data:
        addStocks(
            r.get("stock_id"),
            r.get("name"),
            r.get("stock_price"),
            r.get("market_cap"),
            r.get("sector"),
            r.get("industry"),
            r.get("image_url"),
            r.get("logo_url"),
            r.get("twitter_account"),
            r.get("address"),
            r.get("description"),
            r.get("traded_politicians"),
            r.get("traded_committees"),
        )


with open(Path("data/committees.json")) as f:
    json_data = json.load(f)
    for r in json_data:
        addCommittees(
            r.get("committee_id"),
            r.get("congress"),
            r.get("name"),
            r.get("chamber"),
            r.get("start_date"),
            r.get("url"),
            r.get("chair"),
            r.get("number_of_members"),
            r.get("logo_url"),
            r.get("image_url"),
            r.get("twitter_account"),
            r.get("investments"),
            r.get("members"),
            r.get("stocks_traded"),
        )
