import html
import json
import logging
import os
import re
import sys
from collections import defaultdict
from difflib import SequenceMatcher
from pathlib import Path
from typing import Any, Optional
from urllib.parse import urlparse

from scrape_apis import RAW_RESPONSES_DIR, NitterApi


def string_similarity(a: str, b: str) -> float:
    return SequenceMatcher(a=a.lower(), b=b.lower()).ratio()


class Politicians:
    def __init__(self) -> None:
        self.data: dict[str, dict[str, Any]] = {}
        """Mapping of Politician IDs (e.g. A000001) to properties
        of the politician."""

        try:
            self._read_data()
        except FileNotFoundError as e:
            print(f"Error: could not find {e.filename}")
            print("Run scrape_apis.py first!")
            sys.exit(1)
        self._filter_data()

    def _get_twitter_banner_image(self, handle: str) -> Optional[str]:
        if handle is None:
            return None
        return NitterApi.profile_images(handle)["banner"]

    def _read_data(self) -> None:
        """The ProPublica data has most of the data we want for the
        congress of interest (e.g. 117th).

        The Congress.gov data has all the politicians since the beginning of
        time, so we'll parse it second and only get info for politicians of
        interest from the ProPublica data.
        """
        for file in [
            RAW_RESPONSES_DIR / "ProPublicaCongressApi/117/house/members.json",
            RAW_RESPONSES_DIR / "ProPublicaCongressApi/117/senate/members.json",
        ]:
            chamber = file.parent.stem.capitalize()
            with open(file, "r") as f:
                json_body = json.load(f)
                for result in json_body["results"]:
                    for member in result["members"]:
                        member["chamber"] = chamber
                        self.data[member["id"]] = member

        with open(RAW_RESPONSES_DIR / "CongressGovApi/members.json", "r") as f:
            json_body = json.load(f)
            for member in json_body:
                id = member["bioguideId"]
                if id in self.data:
                    try:
                        self.data[id]["img_url"] = member["depiction"]["imageUrl"]
                    except KeyError:
                        self.data[id]["img_url"] = None
                    first_elected = 2023
                    for chamber in member["served"].values():
                        term: dict[str, int]
                        for term in chamber:  # type: ignore
                            first_elected = min(first_elected, term["start"])
                    self.data[id]["first_elected"] = first_elected
                    self.data[id]["banner_url"] = self._get_twitter_banner_image(
                        self.data[id]["twitter_account"]
                    )

        # TODO: read cash on hand and debt data from opensecrets
        for datum in self.data.values():
            datum["cash_on_hand"] = 0.0
            datum["debt"] = 0.0
            datum["stocks_traded"] = []
            datum["committees_membership"] = []

    def _filter_data(self) -> None:
        keys_to_keep: set[str] = set(
            [
                "politician_id",
                "name",
                "gender",
                "party",
                "date_of_birth",
                "first_elected",
                "cash_on_hand",
                "debt",
                "img_url",
                "banner_url",
                "url",
                "twitter_account",
                "chamber",
                "state",
                "district",
                "stocks_traded",
                "committees_membership",
            ]
        )
        for id, datum in self.data.copy().items():
            # TODO: handle middle name?
            datum["name"] = datum["first_name"]
            if datum["middle_name"]:
                datum["name"] += " " + datum["middle_name"]
            datum["name"] += " " + datum["last_name"]
            if datum["suffix"]:
                datum["name"] += ", " + datum["suffix"]
            datum["politician_id"] = id
            # Convert to AB12 to match HouseStockWatcher format
            if "district" in datum:
                datum["district"] = f"{datum['state']}{datum['district']:>02s}"
            self.data[id] = {k: v for k, v in datum.items() if k in keys_to_keep}

    def update_stocks_traded(self, stocks: "Stocks") -> None:
        # (name, district) -> set of stocks
        # if district is length 2, then the person is a Senator
        map_person_stocks: dict[tuple[str, Optional[str]], set[str]] = defaultdict(set)
        map_person_amount: dict[tuple[str, Optional[str]], float] = defaultdict(float)
        with open(
            RAW_RESPONSES_DIR / "HouseStockWatcherApi/all_transactions.json"
        ) as f:
            json_body = json.load(f)
            for transaction in json_body:
                name = transaction["representative"]
                district = transaction["district"]
                ticker = transaction["ticker"]
                str_amount = re.sub(r"[^ \d]", "", transaction["amount"])
                amount = float(str_amount.split(" ")[0])
                if ticker in stocks.data:
                    map_person_stocks[(name, district)].add(ticker)
                    map_person_amount[(name, district)] += amount

        with open(
            RAW_RESPONSES_DIR / "SenateStockWatcherApi/all_transactions.json"
        ) as f:
            json_body = json.load(f)
            for transaction in json_body:
                if "state" not in transaction:
                    continue
                name = transaction["senator"]
                state = transaction["state"]
                ticker = transaction["ticker"]
                str_amount = re.sub(r"[^ \d]", "", transaction["amount"])
                amount = float(str_amount.split(" ")[0])
                if ticker in stocks.data:
                    map_person_stocks[(name, state)].add(ticker)
                    map_person_amount[(name, state)] += amount

        Member = dict[str, Any]
        debug_unmapped_name_districts: list[tuple[str, str]] = []
        for (name, district), tickers in map_person_stocks.items():
            # Name representations sometimes don't match, so we'll try to match
            # by similarity. TODO: Sometimes the districts also don't match
            # because of redistricting.
            name_similarities: list[tuple[Member, float]] = []
            for member in self.data.values():
                if len(district) == 2:
                    # senator
                    if district == member["state"]:
                        name_similarities.append(
                            (member, string_similarity(name, member["name"]))
                        )
                elif len(district) == 4:
                    # house member
                    if "district" not in member:
                        continue
                    if district == member["district"]:
                        name_similarities.append(
                            (member, string_similarity(name, member["name"]))
                        )

            name_similarities = list(filter(lambda t: t[1] >= 0.66, name_similarities))
            name_similarities = sorted(name_similarities, key=lambda t: t[1])
            # logging.debug(f"{name} {name_similarities}")

            if len(name_similarities) != 0:
                stocks_traded = name_similarities[0][0]["stocks_traded"]
                stocks_traded = sorted(set(stocks_traded + list(tickers)))
                name_similarities[0][0]["stocks_traded"] = stocks_traded
                name_similarities[0][0]["cash_on_hand"] += map_person_amount[
                    (name, district)
                ]
            else:
                debug_unmapped_name_districts.append((name, district))

        if len(debug_unmapped_name_districts) != 0:
            logging.warning(
                f"{len(debug_unmapped_name_districts)} transactions did not match a person"
            )
            for item in debug_unmapped_name_districts:
                logging.debug(item)

    def update_committees_membership(self, committees: "Committees") -> None:
        for committee_id, committee in committees.data.items():
            for politician_id in committee["members"]:
                self.data[politician_id]["committees_membership"].append(committee_id)

    def write_data(self) -> None:
        save_path = Path("data/politicians.json")
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            # we want an array for database import
            export = list(self.data.values())
            json.dump(export, f, indent=4, sort_keys=True)


class Stocks:
    def __init__(self) -> None:
        self.data: dict[str, dict[str, Any]] = {}

        try:
            self._read_data()
        except FileNotFoundError as e:
            print(f"Error: could not find {e.filename}")
            print("Run scrape_apis.py first!")
            sys.exit(1)
        self._filter_data()

    def _read_data(self) -> None:
        with open(RAW_RESPONSES_DIR / "AlphaVantageApi/overviews.json", "r") as f:
            json_body = json.load(f)
            for ticker, stock in json_body.items():
                if stock is not None:
                    self.data[ticker] = stock

        with open(RAW_RESPONSES_DIR / "TwelveDataApi/logos.json", "r") as f:
            with open(RAW_RESPONSES_DIR / "ParseWebsiteData/websites.json", "r") as f2:
                # forgive me for this naming
                json_body = json.load(f)
                json_body2 = json.load(f2)
                for ticker, logo_url in json_body.items():
                    if ticker not in self.data:
                        continue
                    if logo_url is None or len(logo_url) == 0:
                        continue

                    website_url = logo_url.split("/")[-1]
                    if website_url.count(".") == 1:
                        website_url = "https://www." + website_url
                    else:
                        website_url = "https://" + website_url

                    self.data[ticker]["logo_url"] = logo_url
                    self.data[ticker]["image_url"] = None
                    self.data[ticker]["twitter_account"] = None

                    if website_url in json_body2:
                        remapping = [
                            ("avatar", "logo_url"),
                            ("banner", "image_url"),
                            ("twitter_account", "twitter_account"),
                        ]
                        for old_key, new_key in remapping:
                            if old_key in json_body2[website_url]:
                                self.data[ticker][new_key] = json_body2[website_url][
                                    old_key
                                ]

        for datum in self.data.values():
            datum["traded_politicians"] = []
            datum["traded_committees"] = []

    def _filter_data(self) -> None:
        keys_to_keep: set[str] = {
            "stock_id",
            "name",
            "stock_price",
            "market_cap",
            "sector",
            "industry",
            "image_url",
            "logo_url",
            "address",
            "description",
            "traded_politicians",
            "traded_committees",
            "twitter_account",
        }

        for id, datum in self.data.copy().items():
            datum["stock_id"] = datum["Symbol"]
            datum["name"] = datum["Name"]
            datum["stock_price"] = datum["50DayMovingAverage"]
            datum["market_cap"] = datum["MarketCapitalization"]
            datum["sector"] = datum["Sector"]
            datum["industry"] = datum["Industry"]
            datum["address"] = datum["Address"]
            datum["description"] = datum["Description"]

            self.data[id] = {k: v for k, v in datum.items() if k in keys_to_keep}

    def update_traded_politicians_and_committees(
        self, politicians: "Politicians", committees: "Committees"
    ) -> None:
        """The Politicians and Committees need to be updated first."""
        for politician_id, politician in politicians.data.items():
            for ticker in politician["stocks_traded"]:
                self.data[ticker]["traded_politicians"].append(politician_id)

        for committee_id, committee in committees.data.items():
            for ticker in committee["stocks_traded"]:
                self.data[ticker]["traded_committees"].append(committee_id)

    def write_data(self) -> None:
        save_path = Path("data/stocks.json")
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            # we want an array for database import
            export = list(self.data.values())
            json.dump(export, f, indent=4, sort_keys=True)


class Committees:
    def __init__(self) -> None:
        self.data: dict[str, dict[str, Any]] = {}

        try:
            self._read_data()
        except FileNotFoundError as e:
            print(f"Error: could not find {e.filename}")
            print("Run scrape_apis.py first!")
            sys.exit(1)
        self._filter_data()

    def _is_relative_url(self, url: str) -> bool:
        return urlparse(url).netloc == ""

    def _default_logo(self, chamber: Optional[str]) -> str:
        defaults = {
            "house": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Seal_of_the_United_States_House_of_Representatives.svg/480px-Seal_of_the_United_States_House_of_Representatives.svg.png",
            "senate": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Seal_of_the_United_States_Senate.svg/480px-Seal_of_the_United_States_Senate.svg.png",
            "joint": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Seal_of_the_United_States_Congress.svg/481px-Seal_of_the_United_States_Congress.svg.png",
            None: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Seal_of_the_United_States_Congress.svg/481px-Seal_of_the_United_States_Congress.svg.png",
        }
        return defaults[None if chamber is None else chamber.lower()]

    def _get_twitter_profile_images(
        self, handle: Optional[str], chamber: Optional[str] = None
    ) -> dict[str, Optional[str]]:
        if not handle:
            return {
                "logo_url": self._default_logo(chamber),
                "image_url": None,
            }
        images = NitterApi.profile_images(handle)
        return {
            "logo_url": images["avatar"],
            "image_url": images["banner"],
        }

    def _read_data(self) -> None:
        dirs = (
            RAW_RESPONSES_DIR / "ProPublicaCongressApi/117/house/committees",
            RAW_RESPONSES_DIR / "ProPublicaCongressApi/117/senate/committees",
            RAW_RESPONSES_DIR / "ProPublicaCongressApi/117/joint/committees",
        )
        for dir in dirs:
            for file in os.listdir(dir):
                with open(dir / file, "r") as f:
                    json_body = json.load(f)
                    result = json_body["results"]  # there should be only 1
                    committee = result[0]  # again, there should only be 1

                    # handle edge case with Veterans' Affairs
                    committee["name"] = html.unescape(committee["name"])
                    committee["congress"] = int(committee["congress"])
                    committee["members"] = []
                    for member in committee["current_members"]:
                        committee["members"].append(member["id"])
                        # _generally_ the chairman is listed before vice chairman
                        # but this isn't exhaustively verified.
                        if (
                            committee["chair"] is None
                            and member["note"] is not None
                            and "chair" in member["note"].lower()
                        ):
                            committee["chair"] = member["id"]

                    self.data[committee["id"]] = committee

        house_joint_twitter_handles: "dict[str, str]" = {}
        senate_twitter_handles: "dict[str, str]" = {}
        twitter_files = (
            "data/house-committee-twitter.tsv",
            "data/senate-committee-twitter.tsv",
        )
        for file in twitter_files:
            with open(file, "r") as f:
                for line in f:
                    tokens = line.strip().split("\t")
                    if len(tokens) < 2:
                        continue

                    if not tokens[1].startswith("@"):
                        continue

                    chamber_handles = (
                        house_joint_twitter_handles
                        if "house" in file
                        else senate_twitter_handles
                    )
                    if tokens[0] not in chamber_handles:
                        chamber_handles[tokens[0]] = tokens[1]

        for committee in self.data.values():
            name = committee["name"]
            chamber = committee["chamber"]
            handle: Optional[str] = None

            if chamber in ("House", "Joint"):
                name = re.sub("^Committee on (the )?", "", name)
                if name in house_joint_twitter_handles:
                    handle = house_joint_twitter_handles[name]
                else:
                    logging.warning(f"Twitter not found for ({chamber}): {name}")
            else:
                if name in senate_twitter_handles:
                    handle = senate_twitter_handles[name]
                else:
                    logging.warning(f"Twitter not found for (Senate): {name}")
            committee["twitter_account"] = handle
            committee |= self._get_twitter_profile_images(handle, chamber)

        for committee in self.data.values():
            chamber = committee["chamber"]
            id = committee["id"].lower() + "00"
            congress_file = RAW_RESPONSES_DIR / f"CongressGovApi/{chamber}/{id}.json"
            if congress_file.exists():
                with open(congress_file, "r") as f:
                    json_body = json.load(f)
                    try:
                        start_date = json_body["history"][0]["startDate"]
                        start_date = start_date.split("-")[0]
                        committee["start_date"] = int(start_date)
                    except KeyError:
                        pass

        for datum in self.data.values():
            if "start_date" not in datum:
                datum["start_date"] = 2021
            datum["investments"] = 0.0
            datum["stocks_traded"] = []

    def _filter_data(self) -> None:
        keys_to_keep: set[str] = {
            "committee_id",
            "congress",
            "name",
            "chamber",
            "start_date",  # not available through ProPublica
            "url",
            "chair",
            "number_of_members",
            "logo_url",
            "image_url",
            "members",
            "stocks_traded",
            "twitter_account",
            "investments",
        }

        for id, datum in self.data.copy().items():
            datum["committee_id"] = datum["id"]
            datum["number_of_members"] = datum["num_results"]

            self.data[id] = {k: v for k, v in datum.items() if k in keys_to_keep}

    def update_stocks_traded(self, politicians: "Politicians") -> None:
        """Naturally, the Politicians object needs to have been updated with
        stock information already."""
        for committee in self.data.values():
            for member_id in committee["members"]:
                committee["stocks_traded"].extend(
                    politicians.data[member_id]["stocks_traded"]
                )
                committee["investments"] += politicians.data[member_id]["cash_on_hand"]
            committee["stocks_traded"] = sorted(set(committee["stocks_traded"]))

    def write_data(self) -> None:
        save_path = Path("data/committees.json")
        os.makedirs(save_path.parent, exist_ok=True)
        with open(save_path, "w") as f:
            # we want an array for database import
            export = list(self.data.values())
            json.dump(export, f, indent=4, sort_keys=True)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    politicians = Politicians()
    stocks = Stocks()
    committees = Committees()

    politicians.update_stocks_traded(stocks)
    politicians.update_committees_membership(committees)

    committees.update_stocks_traded(politicians)

    stocks.update_traded_politicians_and_committees(politicians, committees)

    politicians.write_data()
    stocks.write_data()
    committees.write_data()
