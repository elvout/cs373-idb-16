const toolchainData = {
    data: [
        {
            name: "Bootstrap",
            imageUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/1280px-Bootstrap_logo.svg.png",
            description: "Frontend CSS framework",
            url: "https://getbootstrap.com/",
        },
        {
            name: "ReactJS",
            imageUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png",
            description: "Frontend Javascript framework",
            url: "https://reactjs.org/",
        },
        {
            name: "Amazon Web Services",
            imageUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1280px-Amazon_Web_Services_Logo.svg.png",
            description: "Website hosting",
            url: "https://aws.amazon.com/amplify/",
        },
        {
            name: "GitLab",
            imageUrl: "https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/144_Gitlab_logo_logos-512.png",
            description: "Git repository hosting and CI/CD",
            url: "https://about.gitlab.com/",
        },
        {
            name: "Postman",
            imageUrl: "https://cdn.worldvectorlogo.com/logos/postman.svg",
            description: "Platform for building APIs",
            url: "https://www.postman.com/",
        },
    ]
}
export default toolchainData;
