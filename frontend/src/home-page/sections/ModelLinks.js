import {Button, Card} from 'react-bootstrap';
import {Link} from "react-router-dom";
import politicianImg from "../../images/politician.jpg"

const models = [

        {
            name: "Stocks",
            desc: "View companies that have donated to politicians and committees.",
            image: "https://bsmedia.business-standard.com/_media/bs/img/article/2021-06/20/full/1624205095-0665.jpg?im=Resize,width=620",
            link: "/stocks"
        },
    
        {
            name: "Politicians",
            desc: "View what stocks politicians own and trade, and see what committees they are a part of.",
            image: "https://cdn.dribbble.com/userupload/3864781/file/original-3fef22b186d017ebffe84b162cc20a89.jpg?compress=1&resize=1504x1002",
            link: "/politicians"
        },
    
        {
            name: "Committees",
            desc: "View what committees companies donate to, and what politicians are a part of them.",
            image: "https://cdn.dribbble.com/users/90628/screenshots/7281430/media/496585126ea1581147b806cf94478462.jpg?compress=1&resize=1600x1200&vertical=top",
            link: "/committees"
        }
    ]


export default function ModelLinks(){
    return(
        <div style={{display:"flex", justifyContent: "center"}}>
              {models.map((model) => {
                return(
                <div style={{width: "28%",  margin: "2.4% 1.9% 1.9% 1.9%"}}>
                    <Card style={{width: "100%", height: "100%"}}>
                    <Card.Img style={{height: "21vw"}} variant="top" src={model.image} />
                    <Card.Body>
                        <Card.Title style={{fontWeight:"600"}}>{model.name}</Card.Title>
                        <Card.Text>{model.desc}</Card.Text>
                        <Link to={model.link}>
                            <Button variant="primary">{model.name}</Button>
                        </Link>
                    </Card.Body>
                    </Card>
                </div>
                )
              })}
        </div>
    );
}